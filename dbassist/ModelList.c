/*******************************************************************
    Copyright (c) [2023] [s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "ModelList.h"


ModelList* initModelList()
{
	ModelList* L = (ModelList*)malloc(sizeof(ModelList));
	L->nLength = 0;
	return L;
}

void addModel(ModelList* L,model* p)
{
	*(L->modelArray + L->nLength) = p;
	L->nLength++;
}

void freeValue(ModelList* modellist)
{
	if(modellist->nLength>0)
  {
  	int i = 0;
	for(i=modellist->nLength-1;i>=0;i--)
	{
	   model* temp = modellist->modelArray[i];
	   if(temp->sqltype==MYSQL_TYPE_STRING||temp->sqltype==MYSQL_TYPE_VAR_STRING)
	   freeVal(temp->data);
	   modellist->modelArray[i] = NULL;
	   modellist->nLength = i;
	}
	printf("\nmodellist->nLength = %d",modellist->nLength);
  }
}
