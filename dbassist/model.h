/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#pragma once

#include "dataType.h"
#include "mysql.h"
#include "common.h"
#include "cJSON.h"

typedef struct model
{
    enum_field_types sqltype;
    dataType dtype;
    unsigned long plength;
    unsigned long col_length;
    int retSelected;
    int parSelected;
    filledTypes filltype; 
    int orgfield;
    long long addr;
	int valueint;
	MYSQL_TIME valuetime;
    double valuedouble;
    long long valuelong;
	float valuefloat;
	char* data;
	char name[NAME_SIZE];
	char alianame[ALIA_NAME_SIZE];
	char fullname[NAME_SIZE];
}model;

JsonNode* convertModelToJson(model** pblist,int nCount);

void fillData(model* pobject);

void fillAllData(model** pblist,int nCount);

void fillBindValFromModel(model** pblist,MYSQL_BIND* bind,int nCount);

void freeVal(void* p);


