/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

#define Max_Index_Size 512


typedef enum indexType{
	Numerics,
	Brackets,
	Braceses,
	DouQuots
}indexType;

typedef struct indexNode
{
	int nLIndex;
	int nRIndex;
    indexType type;
}indexNode;

typedef struct indexList
{
	indexNode* array[Max_Index_Size];
	int length;
	indexType type;
	char lchar;
	char rchar;
}indexList;

indexList* createIndexList(indexType type);

void addMember(indexList* L,indexNode* p);

indexNode* createIndexNode(int nLIndex,int nRIndex,indexType type);

void showIndexList(indexList* L);

void sortList(indexList* L);

int BracketMatch(char *x,indexList* L);

void NumberMatch(char* x,indexList* L);

int getnLIndex(int n,indexList* L);

int getnRIndex(int n,indexList* L);
