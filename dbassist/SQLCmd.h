/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#pragma once

#include <stdarg.h>
#include "TableValue.h"
#include "str_extend.h"
#include "ModelList.h"
#include "SQLObject.h"
#include "common.h"
#include "hashMap.h"
#include "orderObj.h"
#include "cJSON.h"

#define SQL_MAX_SIZE 1024

#define ARR_MAX_SIZE 200


#define count(x)({\
     char* strX = VNAME(x);\
     JsonNode* p = parseTree(strX);\
     if(p!=NULL)\
     strX = Find(p,"paraname")->valuestring;\
	 char* temp = getFunJsonObj(VNAME(count(x)),strX,C_INT);\
	 temp;\
})

#define sum(x)({\
     char* strX = VNAME(x);\
     JsonNode* p = parseTree(strX);\
     if(p!=NULL)\
     strX = Find(p,"paraname")->valuestring;\
	 char* temp = NULL;\
	 if(sizeof(x)==8)\
	    temp = getFunJsonObj(VNAME(sum(x)),strX,C_DOUBLE);\
	 else\
	    temp = getFunJsonObj(VNAME(sum(x)),strX,C_INT);\
	 temp;\
})

#define DATE_FORMAT(date,format)({\
    char* str = VNAME(date);\
    JsonNode* p = parseTree(str);\
    if(p!=NULL)\
    str = Find(p,"paraname")->valuestring;\
	char strTemp[80];\
    memset(strTemp,'\0',80);\
    sprintf(strTemp,"DATE_FORMAT(%s,%s)",str,format);\
	char* temp = getFunJsonObj(strTemp,str,C_VARCHAR);\
    temp;\
})

#define getFunJsonObj(fullname,paraname,ctype)({\
      JsonNode* p = createJsonObject("");\
      addChildString(p,"fullname",fullname);\
      addChildString(p,"paraname",paraname);\
      addChildInteger(p,"ctype",ctype);\
      char* str = printJson(p);\
      str;\
})

#define add_pArr(pcmd,size)({\
    char* pstr = NEWSIZE(char,size);\
    memset(pstr,'\0',size);\
	int nNum = pcmd->nSize;\
	char** q = pcmd->pArr;\
	*(q+nNum) = pstr;\
	pcmd->nSize++;\
	pstr;\
})

#define free_pArr(pcmd)({\
    if(pcmd->nSize>0){\
	int i = 0;\
    for(i=0; i<pcmd->nSize; i++)\
    {\
    	char* strtemp = *(pcmd->pArr + i);\
    	free(strtemp);\
    	strtemp = NULL;\
	}\
	pcmd->nSize = 0;\
  }\
})

#define forVPath(pcmd,obj,name)({\
    int m = 0;\
	char** pdest = getStrArr(name,".",m);\
	int n = strlen(obj->objName)+strlen(pdest[m-1])+2;\
    char* fullname = add_pArr(pcmd,n);\
    sprintf(fullname,"%s.%s",obj->objName,pdest[m-1]);\
    printf("\nfullname = %s\n",fullname);\
	fullname;\
}) 

/*第零步 sql選擇表*/ 
#define selectTable(pcmd,object,name)({\
    TableValue* pVal = (TableValue*)object;\
	if(pVal->fillMap!=NULL)\
	{\
	  pVal->map->removeAll(pVal->map);\
	  pVal->fillMap(object);\
    }\
	if(strlen(pVal->pbValue->aliaName)==0&&name!=NULL){\
	strcat(pVal->pbValue->aliaName,name);\
	Put(pcmd->tValueMap,name,pVal);}\
	char* fullname = add_pArr(pcmd,strlen(pVal->pbValue->tableName)+strlen(pVal->pbValue->aliaName)+2);\
	if(strlen(pVal->pbValue->aliaName)!=0)\
	   sprintf(fullname,"%s %s",pVal->pbValue->tableName,pVal->pbValue->aliaName);\
	else\
	   sprintf(fullname,"%s",pVal->pbValue->tableName);\
	fullname;\
})
 

/*第零步 sql選擇視圖*/ 
#define selectView(pcmd,obj)({\
    memset(obj->objName,'\0',15);\
    sprintf(obj->objName,"view_%c",getViewN());\
    char* fvalue = pcmd->subQuery(obj);\
    char* strname = add_pArr(pcmd,strlen(obj->objName)+strlen(fvalue)+4);\
	sprintf(strname,"%s %s",fvalue,obj->objName);\
	strname;\
})

/*第一步 sql選擇select字段*/ 
#define selectColumn(pcmd,object,name,type,optional)({ \
    char* strname = name;\
	if(findlast(name,"select ")<0)\
	{\
	    int n = 0;\
		TableValue* pVal = (TableValue*)object;\
		char** pdest = getStrArr(name,".",n);\
		model* m = NULL;\
		JsonNode* node = parseTree(name);\
	    if(node==NULL)\
	  {\
		if(n==1)\
	  {\
	    m = (model*)Get(pVal->map,name);\
		if(strlen(m->fullname)==0&&strlen(pVal->pbValue->aliaName)!=0){\
		sprintf(m->fullname,"%s.%s",pVal->pbValue->aliaName,name);}\
		else\
		if(strlen(m->fullname)==0){\
		sprintf(m->fullname,"%s",name);}\
	  }\
        else if(n==2)\
	  {\
	    m = (model*)Get(pVal->map,pdest[1]);\
		if(strlen(m->fullname)==0&&strlen(pVal->pbValue->aliaName)!=0){\
		sprintf(m->fullname,"%s.%s",pVal->pbValue->aliaName,pdest[1]);}\
		else\
		if(strlen(m->fullname)==0){\
		sprintf(m->fullname,"%s",pdest[1]);}\
      }\
      }\
	    else\
	  {\
	        char* fullname = Find(node,"fullname")->valuestring;\
	        char* paraname = Find(node,"paraname")->valuestring;\
	        int ctype = Find(node,"ctype")->valueint;\
	        int mk = 0;\
			char** pdest2 = getStrArr(paraname,".",mk);\
	    	model* md = (model*)Get(pVal->map,pdest2[mk-1]);\
	    	void* vp = NULL;\
            switch (ctype)\
		  {\
		    case C_SHORT:\
			case C_INT:\
			case C_LONG:\
			{\
			  int nTemp;\
			  vp = (void*)&nTemp;\
			}\
			break;\
			case C_FLOAT:\
			case C_DOUBLE:\
			{\
			  double dTemp;\
			  vp = (void*)&dTemp;\
			}\
			break;\
			case C_DATETIME:\
			case C_TIMESTAMP:\
			{\
			  struct tm* tTemp = (struct tm*)malloc(sizeof(struct tm));\
		      tTemp->tm_year = 1980 - 1900;\
		      tTemp->tm_mon = 12;\
		      tTemp->tm_mday = 17;\
		      vp = (void*)tTemp;\
			}\
			break;\
			case C_VARCHAR:\
			case C_CHAR:\
			{\
			  char* sTemp = NEWSIZE(char,md->col_length);\
			  memset(sTemp,'\0',md->col_length);\
			  vp = (void*)sTemp;\
			}\
			break;\
		  }\
	        struct_setExtend(pVal->map,ctype,md->col_length,fullname,vp,FILLED);\
			m = (model*)Get(pVal->map,fullname);\
			m->orgfield = 0;\
			if(m!=NULL&&strlen(m->fullname)==0){\
		    sprintf(m->fullname,"%s",fullname);}\
		    if(optional!=NULL)\
			setMapValue(pVal->map,fullname,optional);\
	    }\
		if(m!=NULL&&strlen(m->alianame)==0){\
	    sprintf(m->alianame,"column_%c",getColN());}\
		if(m!=NULL&&type==TYPEO&&m->retSelected==0){\
		    m->retSelected=1;\
		    addModel(pcmd->retModelList,m);}\
		else\
		if(m!=NULL&&type==TYPEI&&m->parSelected==0){\
		    m->parSelected=1;\
		    addModel(pcmd->parModelList,m);}\
		if(m!=NULL)\
		strname = m->fullname;\
	}\
    strname;\
})



#define strcatValue(pobj,val)({ \
	printf("\nval parm==%s",val);\
	char str2[strlen(val)+7];\
	memset(str2,'\0',strlen(val)+7);\
	int n,m,b=0;\
	n = pobj->fIndex;\
    m = pobj->sIndex;\
    char subtemp[100];\
    memset(subtemp,'\0',100);\
    if(n>=0)\
  {\
    substr(pobj->sqlstr,m+strlen("select"),n-1,subtemp);\
    strTrim(subtemp);\
    int nK = strlen(subtemp);\
	if(nK!=0)\
    strcat(str2,",");\
	strcat(str2,val);\
	strInsert(pobj->sqlstr,n-2,str2);\
	pobj->fIndex+=strlen(str2);\
  }\
	else\
  {\
	substr(pobj->sqlstr,0,strlen(pobj->sqlstr)-1,subtemp);\
	char** pdest = getStrArr(subtemp," ",b);\
	char* tempstr = pdest[b-1];\
	int t = findpos(pobj->sqlstr,tempstr) + strlen(tempstr) - 1;\
	printf("\ntempstr=%s\n",tempstr);\
	int k = t+2;\
	if(*(pobj->sqlstr+k)&&*(pobj->sqlstr+k)!=32)\
	{\
		strcat(str2,",");\
	}\
	else\
	{\
		strcat(str2," ");\
	}\
	strcat(str2,val);\
	strcat(pobj->sqlstr,str2);\
	printf("\nval2 = %s",str2);\
  }\
}) 

typedef enum condTypes{
   WHERE,ON,HAVING
}condTypes;

typedef enum joinTypes{
   INNER_JOIN,LEFT_JOIN,RIGHT_JOIN,CROSS_JOIN	
}joinTypes;

typedef enum IOTypes{
   TYPEI,TYPEO,TYPEN	
}IOTypes;

typedef enum fromTypes{
   TABLE,VIEW	
}fromTypes;

typedef enum selectTypes{
   DISTINCT,ALL	
}selectTypes;

typedef struct SQLCmd{
    SQLObject* pObj;
	ModelList* parModelList;
    ModelList* retModelList;
    char* pArr[ARR_MAX_SIZE];
    int nSize;
    HashMap tValueMap; //存放表的容器視圖 
    struct SQLCmd* (*multi_selected)(struct SQLCmd* pcmd,void* p,SQLObject* pobj,IOTypes type,char* name,...);//select 
	struct SQLCmd* (*selectFull)(struct SQLCmd* pcmd,void* p,SQLObject* pobj,fromTypes ftype,IOTypes type);
	struct SQLCmd* (*selectAll)(struct SQLCmd* pcmd,void* p,SQLObject* pobj,fromTypes ftype,IOTypes type);
	struct SQLCmd* (*from)(struct SQLCmd* pcmd,SQLObject* pobj,char* value1,...);//from
	struct SQLCmd* (*on)(struct SQLCmd* pcmd,SQLObject* pobj,char* value1,...);//on
	struct SQLCmd* (*values)(struct SQLCmd* pcmd,SQLObject* pobj);//on
	struct SQLCmd* (*fromJoin)(struct SQLCmd* pcmd,SQLObject* pobj,joinTypes type,char* value1,char* value2);
	char* (*forTable)(struct SQLCmd* pcmd,void* p,char* name);//table name and alia select
	char* (*forView)(struct SQLCmd* pcmd,SQLObject* subobj);//view for multi_table
	struct SQLCmd* (*createStr)(struct SQLCmd* pcmd,SQLObject* pobj,crudTypes type,char* format);//創建sql字符串 
    struct SQLCmd* (*onCondition)(struct SQLCmd* pcmd,SQLObject* pobj,condTypes type,char* value,...);//where條件 
	struct SQLCmd* (*group_By)(struct SQLCmd* pcmd,SQLObject* pobj,char* value);//group by條件
	struct SQLCmd* (*order_By)(struct SQLCmd* pcmd,SQLObject* sqlobj,orderObj* pObj,...);//order by條件
	struct SQLCmd* (*limit)(struct SQLCmd* pcmd,SQLObject* sqlobj,int pageNo,int pageSize);//order by條件
	char* (*compare)(struct SQLCmd* pcmd,char* val1,char* operation,char* val2);//A = B
    char* (*fillvalue)(struct SQLCmd* pcmd,void* p,char* name,char* operation,void* optional);//C = ?
    char* (*jointWords)(struct SQLCmd* pcmd,char* words);
	char* (*subQuery)(SQLObject* pobj);
	char* (*forViewPath)(struct SQLCmd* pcmd,SQLObject* pobj,char* name);
	char* (*selectFormat)(selectTypes type);
}SQLCmd;

SQLCmd* createSQLCmd();
void resetCmd(struct SQLCmd* pcmd);
