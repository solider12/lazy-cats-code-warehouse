/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/



#include "model.h"

JsonNode* convertModelToJson(model** pblist,int nCount)
{
	JsonNode* cJObject = createJsonObject("");
	int iCnt = 0;
    model* pObject = NULL;
    if(nCount>0){
	for(iCnt=0; iCnt<nCount; iCnt++)
	{
		   pObject = *(pblist+iCnt);
		   if(pObject->sqltype==MYSQL_TYPE_STRING||pObject->sqltype==MYSQL_TYPE_VAR_STRING)
	   	{
		   	addChildString(cJObject,pObject->fullname,(char*)(intptr_t)pObject->data);
		}
		   else
	    {
		   if(pObject->sqltype==MYSQL_TYPE_TIMESTAMP)
		   {
		   	 addChildString(cJObject,pObject->fullname,(char*)(intptr_t)format_myTime(&pObject->valuetime,"%Y-%m-%d"));
		   }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_DATETIME)
		   {
		     addChildString(cJObject,pObject->fullname,(char*)(intptr_t)format_myTime(&pObject->valuetime,"%Y-%m-%d"));	
		   }
		   else	
		   if(pObject->sqltype==MYSQL_TYPE_LONG)
		   {
		     addChildInteger(cJObject,pObject->fullname,pObject->valueint);
	 	   }
	       else
	       if(pObject->sqltype==MYSQL_TYPE_SHORT)
	       {
			 addChildInteger(cJObject,pObject->fullname,pObject->valueint);
		   }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_FLOAT)
		   {
	       	 addChildDouble(cJObject,pObject->fullname,(double)pObject->valuefloat);  
	       }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_DOUBLE)
		   {
	       	 addChildDouble(cJObject,pObject->fullname,pObject->valuedouble);
		   }
		}
	}
  }
  return cJObject;
}

void fillData(model* pobject)
{
    	//printf("\nfilldata begin===========================================================");
		pobject->plength = 0;
		if(pobject->sqltype==MYSQL_TYPE_STRING||pobject->sqltype==MYSQL_TYPE_VAR_STRING)
	  {
	       CopyString(pobject->data,(char*)(intptr_t)pobject->addr,pobject->col_length);
	       pobject->plength = strlen((char*)pobject->data);
	       //printf("\npobject->data = %s",pobject->data);
		   //printf("\npobject->data = %s",pobject->data);
	  }
	    else
	  {
	      if(pobject->sqltype==MYSQL_TYPE_LONG||pobject->sqltype==MYSQL_TYPE_SHORT)
	     {
	        pobject->valueint = *(int*)(intptr_t)pobject->addr;
	        //printf("\nbest herererer!!");
	        //printf("\npobject->valueint = %d",pobject->valueint);
	     }
	      else
	      if(pobject->sqltype==MYSQL_TYPE_FLOAT)
	     {
	        pobject->valuefloat = *(float*)(intptr_t)pobject->addr;
	     }
		 else
	     if(pobject->sqltype==MYSQL_TYPE_LONGLONG)
	     {
	        pobject->valuelong = *(long*)(intptr_t)pobject->addr;
	     }  
		 else
	     if(pobject->sqltype==MYSQL_TYPE_DOUBLE)
	     {
	        pobject->valuedouble = *(double*)(intptr_t)pobject->addr;
	        //printf("\npobject->valuedouble = %f",pobject->valuedouble);
	     }
		 else
		 if(pobject->sqltype==MYSQL_TYPE_DATETIME)
		 {
		 	tm_to_mysql_time(&pobject->valuetime,(struct tm*)(intptr_t)pobject->addr);
		 }
		 else
		 if(pobject->sqltype==MYSQL_TYPE_TIMESTAMP)
		 {
		    tm_to_mysql_time(&pobject->valuetime,(struct tm*)(intptr_t)pobject->addr);
		 }
	  }
	  //printf("\nfilldata end===========================================================");
}


void fillBindValFromModel(model** pblist,MYSQL_BIND* bind,int nCount)
{
	int iCnt = 0;
    model* pObject = NULL;
    if(nCount>0){
	for(iCnt=0; iCnt<nCount; iCnt++)
	{
		   pObject = *(pblist+iCnt);
		   bind[iCnt].buffer_type = pObject->sqltype;
		   //printf("\npObject->alianame = %s\n",pObject->alianame);
		   //printf("\npObject->name = %s\n",pObject->name);
		   //printf("\npObject->fullname = %s\n",pObject->fullname);
		   if(pObject->sqltype==MYSQL_TYPE_STRING||pObject->sqltype==MYSQL_TYPE_VAR_STRING)
	   	{
		    bind[iCnt].buffer=(char*)pObject->data;//
		    bind[iCnt].buffer_length = pObject->col_length;
		    bind[iCnt].length=&pObject->plength;
		    bind[iCnt].is_null=0;
		}
		   else
	    {
		   if(pObject->sqltype==MYSQL_TYPE_TIMESTAMP)
		   {
			 bind[iCnt].buffer=(char*)&pObject->valuetime;
			 bind[iCnt].length=0;
		     bind[iCnt].is_null=0;
		   }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_DATETIME)
		   {
		     bind[iCnt].buffer=(char*)&pObject->valuetime;
			 bind[iCnt].length=0;
		     bind[iCnt].is_null=0;
		   }
		   else	
		   if(pObject->sqltype==MYSQL_TYPE_LONG)
		   {
		     bind[iCnt].buffer=(char*)&pObject->valueint;
		     bind[iCnt].length=0;
		     bind[iCnt].is_null=0;
	 	   }
	       else
	       if(pObject->sqltype==MYSQL_TYPE_SHORT)
	       {
			 bind[iCnt].buffer=(char*)&pObject->valueint;
			 bind[iCnt].length=0;
		     bind[iCnt].is_null=0;
		   }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_FLOAT)
		   {
	       	 bind[iCnt].buffer=(char*)&pObject->valuefloat;// ?  ?  ?
			 bind[iCnt].length=0;
		     bind[iCnt].is_null=0;  
	       }
		   else
		   if(pObject->sqltype==MYSQL_TYPE_DOUBLE)
		   {
	       	 bind[iCnt].buffer=(char*)&pObject->valuedouble;
	       	 bind[iCnt].length=0;
		     bind[iCnt].is_null=0;
		   }
		}

    }
  }
}

void fillAllData(model** pblist,int nCount)
{
	int iCnt = 0;
	model* pObject = NULL;
    if(nCount>0){
	for(iCnt=0; iCnt<nCount; iCnt++)
	{
	   pObject = *(pblist+iCnt);
	   fillData(pObject);
	}
  }
}


void freeVal(void* p)
{
	if(p!=NULL)
	{
	  free(p);
	  p = NULL;
	}
}
