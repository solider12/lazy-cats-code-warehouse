/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#include "salgrade.h"



static void fillMap(void* pdata)
{
     salgrade* pvalue = (salgrade*)pdata;

	 struct_set(pvalue->tValue.map,C_INT,11,pvalue->grade,&pvalue->grade,NOFILLED);
	 
	 struct_set(pvalue->tValue.map,C_INT,11,pvalue->lowsal,&pvalue->lowsal,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_INT,11,pvalue->highsal,&pvalue->highsal,FILLED);
}



static void initMembers(void* pdata)
{
	salgrade* pvalue = (salgrade*)pdata;
	pvalue->grade = 0;
	pvalue->lowsal = 0;
    pvalue->highsal = 0;
}

static void initValue(void* pdata)
{
	salgrade* pvalue = (salgrade*)pdata;
	pvalue->tValue.map = createHashMap(NULL, NULL);
	pvalue->tValue.pbValue = NEW(TableBasic);
	pvalue->tValue.fillBasic = fillBasic;
	pvalue->tValue.fillMap = fillMap;
	pvalue->tValue.print = print;
	pvalue->tValue.initMembers = initMembers;
}

salgrade createSalgrade()
{
	salgrade p;
	p.tValue.initValue = initValue;
	p.tValue.initValue(&p);
	p.tValue.fillBasic(&p,VNAME(salgrade));
	//p.tValue.fillMap(&p);
	p.tValue.initMembers(&p);
	p.tValue.print(&p);
	return p;
}

