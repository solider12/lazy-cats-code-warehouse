#include "util.h"

int isNumber(char* str) {
    int i = 0;
    
    // 处理正负号
    if (str[i] == '-' || str[i] == '+') {
        i++;
    }
    
    // 遍历判断每个字符是否为数字字符
    while (str[i] != '\0') {
        if (!isdigit(str[i])) {
            return 0;
        }
        i++;
    }
    return 1;
}

int isNumeric3(char* str) {
        //3 作标记把所有可能的情况都考虑到
        int sign=0;//+ -是否出现过
        int Ee=0;//E e 是否出现过
        int decimal=0;//小数点是否出现过
        int len=strlen(str);
        int i;
		for(i=0;i<len;i++) {
            if(str[i]=='E'||str[i]=='e') {
                //e后面一定要有数字
                if(i==len-1) {
                    return 0;
                }
                if(Ee) {//不能存在两个E
                    return 0;
                }
                Ee=1;
            } else if(str[i]=='+'||str[i]=='-') {
                //第二次出现+ -符号必须跟在E后面
                if(sign&&str[i-1]!='e'&&str[i-1]!='E') {
                    return 0;
                }
                //第一次出现+ -号且不是在字符串开头，也必须紧跟在e后面
                if(!sign&&i>0&&str[i-1]!='e'&&str[i-1]!='E') {
                    return 0;
                }
                sign=1;
            } else if(str[i]=='.') {
                //e后面不能接小数点 小数点不能出现两次
                if(decimal||Ee) {
                    return 0;
                }
                decimal=1;
            } else if(str[i]<'0'||str[i]>'9') {//不合法字符
                return 0;
            }
        }
        return 1;
}

char * createUUID() {
	const char * c = "89ab";
	char * buf = (char *)malloc(37);
	char * p = buf;
    int n;
	for (n = 0; n < 16; ++n) {
		int b = rand() % 255;

		switch (n) {
			case 6: sprintf(p, "4%x", b % 15); break;
			case 8: sprintf(p, "%c%x", c[rand() % strlen(c)], b % 15); break;
			default: sprintf(p, "%02x", b); break;
		}

		p += 2;

		switch (n) {
			case 3:
			case 5:
			case 7:
			case 9:
				*p++ = '-'; break;
		}
	}
	*p = 0;
	return buf;
}

int getNumberLength(int number) {
	if (number < 0) return 0;

	if (number == 0) return 1;

	int length = 0;

	while (number != 0) {
		number /= 10;
		length++;
	}

	return length;
}

void  copyTmValue(struct tm* tm1,struct tm* tm2){
    tm1->tm_sec = tm2->tm_sec;
    tm1->tm_min = tm2->tm_min;
    tm1->tm_hour = tm2->tm_hour;
    tm1->tm_mday = tm2->tm_mday;
    tm1->tm_mon = tm2->tm_mon;
    tm1->tm_year = tm2->tm_year;
    tm1->tm_wday = tm2->tm_wday;
    tm1->tm_yday = tm2->tm_yday;
    tm1->tm_isdst = tm2->tm_isdst;
}

int gettimeofday(struct timeval *tp, void *tzp)
{
	time_t clock;
	struct tm tm;
	SYSTEMTIME wtm;
	GetLocalTime(&wtm);
	tm.tm_year = wtm.wYear - 1900;
	tm.tm_mon = wtm.wMonth - 1;
	tm.tm_mday = wtm.wDay;
	tm.tm_hour = wtm.wHour;
	tm.tm_min = wtm.wMinute;
	tm.tm_sec = wtm.wSecond;
	tm.tm_isdst = -1;
	clock = mktime(&tm);
	tp->tv_sec = clock;
	tp->tv_usec = wtm.wMilliseconds * 1000;
	return (0);
}
