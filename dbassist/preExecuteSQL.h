/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#pragma once

#include "SQLCmd.h"

/*初始化mysql句柄*/ 
MYSQL* init_mysql();

/*连接mysql数据库*/
MYSQL* connect_db(MYSQL *conn_prt,char* host,char* user,char* pwd,char* db);

/*預處理執行sql語句*/ 
JsonNode* prepare_executeSQL(MYSQL *mysql,char* str,ModelList* plist,ModelList* rlist);
