/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#pragma once
#include "common.h"
#include <stdarg.h>

typedef enum sqlTypes
{
   MAINVIEW,SUBVIEW,COLUMN
}sqlTypes;

typedef enum crudTypes{
   ADD,DEL,PUT,GET	
}crudTypes;

typedef struct SQLObject
{
   char* sqlstr;
   sqlTypes type;
   char objName[40]; //viewҕ�D�����ֶ���  
   char* columns[40];
   int colnums;
   int fIndex;
   int sIndex;
   int selectednum;
   crudTypes crudtype;
}SQLObject; 

SQLObject* createSQLObject(int size,sqlTypes type);

void addcolumn(SQLObject* pObj,char* p);


