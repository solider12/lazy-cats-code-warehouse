#pragma once
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include "common.h"
#include "mysql.h"

void mysql_time_to_tm(struct tm* t_time,MYSQL_TIME* m_time);
void tm_to_mysql_time(MYSQL_TIME* m_time,struct tm* t_time);
void tm_to_tm(struct tm* d_time,struct tm* s_time);
long get_current_timestamp();
long get_time_difference(long start_time,long end_time);
char* get_time_by_timestamp(long timestamp);
char* format_time(long timestamp,char* format);
char* format_tm(struct tm* t_time,char* format);
char* format_myTime(MYSQL_TIME* st,char* format);
int standard_to_stamp(char *str_time);
int get_current_year();
int get_current_month();
int get_current_day();
int get_current_hour();
int get_current_minute();
int get_current_second();
int get_current_week();
long get_time_difference_second(long start_time,long end_time);
long get_time_difference_minute(long start_time,long end_time);
long get_time_difference_hour(long start_time,long end_time);
long get_time_difference_day(long start_time,long end_time);
long get_time_difference_month(long start_time,long end_time);
long get_time_difference_year(long start_time,long end_time);
long add_time(long timestamp,int seconds);
long sub_time(long timestamp,int seconds);
long add_week(long timestamp,int week);
long add_second(long timestamp,int second);
long add_minute(long timestamp,int minute);
long add_hour(long timestamp,int hour);
long add_day(long timestamp,int day);
long add_month(long timestamp,int month);
long add_year(long timestamp,int year);
int compare_time(long timestamp1,long timestamp2);
char* week_to_str(int week);
int is_leap_year(int year);
char* get_season_by_timestamp(long timestamp);
long get_first_day_of_month();
long get_first_day_of_month_by_month(int month);
long get_last_day_of_month_by_month(int month);
long get_first_day_of_month_by_year(int year);
long get_last_day_of_month_by_year(int year);
long get_first_day_of_week_by_timestamp(long timestamp);
long get_last_day_of_week_by_timestamp(long timestamp);
int get_day_of_month();
int get_day_of_year();
int get_day_of_week();
int get_day_of_season();
long get_start_time_of_day(long timestamp);
long get_end_time_of_day(long timestamp);

