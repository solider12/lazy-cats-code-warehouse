/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#include "parseStack.h"


indexList* createIndexList(indexType type)
{
	indexList* L = (indexList*)malloc(sizeof(indexList));
	L->length = 0;
	L->type = type;
	switch (L->type)
{
    case Numerics:
	{
		L->lchar = '0'; 
        L->rchar = '9'; 		
	}		
    break;
    case Brackets:
	{
		L->lchar = '['; 
        L->rchar = ']'; 		
	}		
	break;
	case Braceses:
	{
		L->lchar = '{'; 
        L->rchar = '}'; 		
	}		
	break;
	case DouQuots:
	{
		L->lchar = '"'; 
        L->rchar = '"'; 		
	}		
	break;
}	
	return L;
}

void addMember(indexList* L,indexNode* p)
{
	*(L->array + L->length) = p;
    L->length++;
}

void showIndexList(indexList* L)
{
	int i = 0;
	for(i=0; i<L->length; i++)
	{
	   indexNode* p = *(L->array + i);
	   printf("\nlindex=%d\n",p->nLIndex);
	   printf("\nrindex=%d\n",p->nRIndex);	
	}
}

void sortList(indexList* L)
{
	if(L->length>0){
	int i,j;
	for(i=0; i<L->length-1; i++){
        for(j=1; j<L->length-i;j++)
      {
         indexNode* p = *(L->array + j);
         indexNode* q = *(L->array + (j - 1));
		 if(q->nLIndex>p->nLIndex)
        {
		   indexNode* t = *(L->array + (j - 1));
           *(L->array + (j - 1)) = *(L->array + j);
           *(L->array + j) = t;
		}
	  }
	}
	
  }
}

indexNode* createIndexNode(int nLIndex,int nRIndex,indexType type)
{
	indexNode* p = (indexNode*)malloc(sizeof(indexNode));
	p->nLIndex = nLIndex;
	p->nRIndex = nRIndex;
	p->type = type;
    return p; 
}

int BracketMatch(char *x,indexList* L)   //括号匹配
{
    SeqStack S;
	InitStack(&S,sizeof(indexNode*));                   //初始化栈S
    int len = strlen(x);        //获取字符串x的长度
    int i;
	for(i = 0;i < len;i++)       //遍历字符串x中的每个字符
    {
    	indexNode* p;
		p = createIndexNode(-1,-1,L->type);
        if(L->lchar!=L->rchar)
	  {
		if(x[i]==L->lchar)                //如果字符是左括号
        {
            p->nLIndex = i;
			Push(&S,p);                              //将左括号入栈
        }
        else
		if(x[i]==L->rchar)                           //如果字符是右括号
        {
            if(IsEmpty(&S))             //如果栈为空，说明没有匹配的左括号，返回False
                return False;
            Pop(&S,p);
            p->nRIndex = i;
            addMember(L,p); 
	    }
      }
      else
      {
      	if((S.top+1)%2==0&&x[i]==L->lchar)
      	{
      	   p->nLIndex = i;
		   Push(&S,p);          	
		}
		else
      	if((S.top+1)%2==1&&x[i]==L->rchar)
      	{
            if(IsEmpty(&S))             //如果栈为空，说明没有匹配的左括号，返回False
                return False;
            Pop(&S,p);
            p->nRIndex = i;
            addMember(L,p); 
	    }
      }
    }
    return IsEmpty(&S);       //遍历完字符串后，如果栈为空，说明所有括号都匹配，返回True；否则，返回False
}

void NumberMatch(char* x,indexList* L)
{
	char* q = x;
	while(*x)
  {
    if((*x==':'||*x=='['||*x==',')&&*(x+1)!=':'&&*(x+1)!='"'&&*(x+1)!='['&&*(x+1)!='{'&&*(x+1)!=',')
    {
       indexNode* p = createIndexNode(-1,-1,L->type);
       ++x;
       p->nLIndex = (x - q);
       while(*x!=','&&*x!=']'&&*x!='}'&&*x!='"')
	 {
	   	++x;
	 }
	   p->nRIndex = (x - q) - 1;
	   addMember(L,p);     	
    }	
	else
    ++x;
  }
}

int getnLIndex(int n,indexList* L)
{
    if(L->length>0)
   {
   	 int i = 0;
	 for(i=L->length-1; i>=0; i--)
	 {
	    if(L->array[i]->nRIndex==n)
		return L->array[i]->nLIndex;
	 }
   }	
    return -1;
} 

int getnRIndex(int n,indexList* L)
{
    if(L->length>0)
   {
   	 int i = 0;
	 for(i=0; i<L->length; i++)
	 {
	    if(L->array[i]->nLIndex==n)
		return L->array[i]->nRIndex;
	 }
   }	
    return -1;
}
