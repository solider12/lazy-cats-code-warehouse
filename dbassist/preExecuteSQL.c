/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#include "preExecuteSQL.h"

/*初始化mysql句柄*/ 
MYSQL* init_mysql()
{
   return mysql_init(NULL);  	
}

/*连接mysql数据库*/
MYSQL* connect_db(MYSQL *conn_prt,char* host,char* user,char* pwd,char* db)
{
	/*假设我的云服务器IP为118.89.20.60，密码为123456,进入的数据库名字为zje*/
	conn_prt = mysql_real_connect(conn_prt,host,user,
		pwd,db,0,NULL,0);
	if(conn_prt==NULL)	
	{
		printf("failed to connect:%s\n",mysql_error(conn_prt));
		return NULL;
	}
	printf("connect success!\n");
	return conn_prt;
}

JsonNode* prepare_executeSQL(MYSQL *mysql,char* str,ModelList* plist,ModelList* rlist)
{
	MYSQL_STMT  *stmt;//预处理的句柄
	int  param_count, colNum, t;
	int  i = 0;
	MYSQL_BIND  pbind[plist->nLength];//绑定变量
	MYSQL_BIND  rbind[rlist->nLength];//绑定变量
	MYSQL_RES   *prepare_meta_result;
	MYSQL_ROW row;
	
	stmt = mysql_stmt_init(mysql); //预处理的初始化
	if (!stmt)
	{
	  fprintf(stderr, " mysql_stmt_init(), out of memory\n");
	  exit(0);
	}
	if (mysql_stmt_prepare(stmt, str, strlen(str))) //insert 语句 的预处理 
	{
	  fprintf(stderr, " mysql_stmt_prepare(), exec failed\n");
	  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
	  exit(0);
	}
	fprintf(stdout, " prepare, exec successful\n");
	
	
	param_count= mysql_stmt_param_count(stmt);//获得参数个数 
	fprintf(stdout, " total parameters in exec: %d\n", param_count);
	if(param_count>0){
		printf("\nparam_count==%d",param_count);
		printf("\nplist->nLength==%d",plist->nLength);
		if (param_count != plist->nLength) /* validate parameter count */
		{
		  fprintf(stderr, " invalid parameter count returned by MySQL\n");
		  exit(0);
		}
	    memset(pbind, 0, sizeof(pbind));
	    fillBindValFromModel(plist->modelArray,pbind,plist->nLength);
	    printf("\naaaaaaaaaa");
	    printf("plist->nLength=%d",plist->nLength);
	    if (mysql_stmt_bind_param(stmt, pbind)) //绑定变量 参数绑定
		{
		  fprintf(stderr, " mysql_stmt_bind_param() failed\n");
		  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
		  exit(0);
		}
	//	printf("\nend there!!!");
	    fillAllData(plist->modelArray,plist->nLength);
	//    printf("\nend here!!!");
    }
	//printf("\nend there!!!");
	
	if(mysql_stmt_execute(stmt)) //预处理的执行,第一次执行 
	{
	  fprintf(stderr, " mysql_stmt_execute(), 1 failed\n");
	  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
	  exit(0);
	}
	//printf("\nend here!!!");
	
	if(rlist->nLength==0)
	   return NULL;
	prepare_meta_result = mysql_stmt_result_metadata(stmt);
	if (!prepare_meta_result)
	{
	  fprintf(stderr,
	         " mysql_stmt_result_metadata(), \
	           returned no meta information\n");
	  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
	  exit(0);
	}
	/* Get total columns in the query */
	colNum = mysql_num_fields(prepare_meta_result);
	fprintf(stdout,
	        " total columns in SELECT statement: %d\n",
	        colNum);
	
	for(i=0; i<colNum; i++)
	{
		MYSQL_FIELD* filed = mysql_fetch_field_direct(prepare_meta_result,i);
		int j;
		for(j=i; j<rlist->nLength; j++)
		{
			model* m = rlist->modelArray[j];
			char col_name[80];
			memset(col_name,'\0',80);
			if(m->orgfield==1){
			   memcpy(col_name,filed->table,strlen(filed->table));
               strcat(col_name,".");
		    }
		    strcat(col_name,filed->name);
		   	printf("\nm->fullname==%s",m->fullname);
		   	printf("\ncol_name==%s",col_name);
			if(stricmp(col_name,m->fullname)==0)
			{
				model* t = rlist->modelArray[i];
				rlist->modelArray[i] = rlist->modelArray[j];
				rlist->modelArray[j] = t;
				break;
			}
		}
	}
	
	if(rlist->nLength>0){
		memset(rbind,0,sizeof(rbind));
		fillBindValFromModel(rlist->modelArray,rbind,rlist->nLength);
		printf("rlist->nLength=%d",rlist->nLength);
		printf("\neeeebbbbbbbbb");
	    if (mysql_stmt_bind_result(stmt, rbind)) //绑定变量 参数绑定
		{
		  fprintf(stderr, " mysql_stmt_bind_result() failed\n");
		  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
		  exit(0);
		}
	}
    //printf("\nccccccccc");
	
	if (mysql_stmt_store_result(stmt))
	{
	  fprintf(stderr, " mysql_stmt_store_result() failed/n");
	  fprintf(stderr, " %s\n", mysql_stmt_error(stmt));
	  exit(0);
	}

    /* Fetch all rows */
	fprintf(stdout, "Fetching results ...\n");
	//printf("opopoppp");
    JsonNode* cJObject = createJsonArray("array");
	i = 0;
	//int nStatus = mysql_stmt_fetch(stmt);
	//printf("\nnStatus=%d",nStatus);
	
	while(!mysql_stmt_fetch(stmt))
	{
		addChildJson(cJObject,convertModelToJson(rlist->modelArray,rlist->nLength));
		++i;
	}
	fprintf(stdout, " total rows fetched: %d\n", i);
	
	if (i == 0)
	{
	   fprintf(stderr, " MySQL failed to return all rows\n");
	}
	else
	{
	   printf("\njson str = %s",printJson(cJObject));	
	}
    /* Free the prepared result metadata */
	
	mysql_free_result(prepare_meta_result);
	prepare_meta_result = NULL;
	/* Close the statement */
	if (mysql_stmt_close(stmt))
	{
	  /* mysql_stmt_close() invalidates stmt, so call          */
	  /* mysql_error(mysql) rather than mysql_stmt_error(stmt) */
	  fprintf(stderr, " failed while closing the statement\n");
	  fprintf(stderr, " %s\n", mysql_error(mysql));
	  exit(0);
	}
	return cJObject;    
} 
