#include <dos.h>
#include "emp.h"
#include "dept.h"
#include "salgrade.h"
#include "preExecuteSQL.h"


#define _HOST_ "localhost"  //主机
#define _USER_ "root"       //mysql用户,非主机
#define _PASSWD_ "12345678"   //密码
#define _DBNAME_ "mydb"    //库名



//void queryExecute(MYSQL *conn_prt,char* queryStr,char** arr,int* nRow,int* nCol)
//{
//    MYSQL_RES *res;
//    MYSQL_ROW row;
//    int i = 0;
//    int t = mysql_real_query(conn_prt,queryStr,strlen(queryStr));
//	if(t)
//	{
//		printf("failed to query:%s\n",mysql_error(conn_prt));
//		return ;
//	}
//	printf("query success!\n");
// 
//	res = mysql_store_result(conn_prt);
//	int num = *nCol = mysql_num_fields(res);
//	while(row = mysql_fetch_row(res))
//	{
//		for(t = 0;t<num;t++)
//		{
//			printf("%s\t",row[t]);
//			*(arr+i*num+t)=row[t];
//		}
//		printf("\n");
//		i++;
//	}
//	*nRow = i;
//	
//}

//void searchUsers(emp* p,MYSQL *mysql) {
//   MYSQL_STMT *stmt;
//   MYSQL_BIND bind[1];
//   MYSQL_BIND rind[1];
//   MYSQL_ROW row;
//   int empno,t;
//
//   // 初始化参数绑定
//   stmt = mysql_stmt_init(mysql);
//   mysql_stmt_prepare(stmt, "SELECT a.* FROM emp a", strlen("SELECT a.* FROM emp a"));
//   
////   // 绑定参数
////   empno = p->empno;
////   memset(bind, 0, sizeof(bind));
////   bind[0].buffer_type = MYSQL_TYPE_LONG;
////   bind[0].buffer = (void*)&empno;
////   bind[0].is_null = 0;
////   bind[0].length = 0;
////   mysql_stmt_bind_param(stmt, bind);
////   printf("\ncatch small demo");
//   
//   mysql_stmt_execute(stmt);
//   
//   memset(rind, 0, sizeof(rind));
//   rind[0].buffer_type = MYSQL_TYPE_LONG;
//   rind[0].buffer = (void*)&empno;
//   rind[0].is_null = 0;
//   rind[0].length = 0;
//   mysql_stmt_bind_result(stmt,rind);
//
//   // 执行预处理语句并遍历结果
//   
//   int k = mysql_stmt_store_result(stmt);
//   MYSQL_RES* res = mysql_stmt_result_metadata(stmt);
//   int num = mysql_num_fields(res);
//   
//   while(!mysql_stmt_fetch(stmt))
//  {
//	    printf("\n000000111111");
////		for(t = 0;t<num;t++)
////		{
////			printf("%s\t",row[t]);
////		}
//        printf("\nempno==%d",empno);
//		printf("\n");
//  }
//   mysql_stmt_close(stmt);
//}


int  main()
{
	    system("cls");
	    MYSQL* conn_prt = init_mysql();
		conn_prt = connect_db(conn_prt,_HOST_,_USER_,_PASSWD_,_DBNAME_);
        emp a = createEmp();
        dept b = createDept();
        strcpy(b.loc,"New York");
        printf("\nb.loc==%s",b.loc);
        a.deptno = 20;
        salgrade c = createSalgrade();
	    c.grade = 14;
	    SQLCmd* pCmd = createSQLCmd();
	    a.sal = 1200.00;
	    double salK = 8500.00;
//	    pCmd->createStr(pCmd,pCmd->pObj,GET,NULL)
//	        ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
//	        ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.deptno),sum(a.sal),"")
//	        ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->fillvalue(pCmd,&a,VNAME(a.sal),">",NULL),"")
//	        ->group_By(pCmd,pCmd->pObj,VNAME(a.deptno))
//	        ->onCondition(pCmd,pCmd->pObj,HAVING,pCmd->fillvalue(pCmd,&a,sum(a.sal),">",&salK),"")
//	        ->order_By(pCmd,pCmd->pObj,createOrderObj("a.deptno",DESC),NULL);
        pCmd->createStr(pCmd,pCmd->pObj,GET,NULL)
            ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
            ->selectAll(pCmd,&a,pCmd->pObj,TABLE,TYPEO)
            ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->fillvalue(pCmd,&a,DATE_FORMAT(a.hiredate,"'%Y-%m-%d'"),">","1980-12-17"),"")
            ->limit(pCmd,pCmd->pObj,1,3);
        
		free_pArr(pCmd);	
        printf("\npppppppppppppppppppppppppppppppppppppppppppcmd.str = %s",pCmd->pObj->sqlstr);
        //b.deptno = 30;
        printf("\npppppppppppppppppppppppppppppppppppa.empno=%d\n",a.empno);
		JsonNode* Jnode1 = prepare_executeSQL(conn_prt,pCmd->pObj->sqlstr,pCmd->parModelList,pCmd->retModelList);
        system("pause");
        resetCmd(pCmd);
		a.empno = 7369;
		pCmd->createStr(pCmd,pCmd->pObj,GET,NULL)
	        ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
	        ->selectFull(pCmd,&a,pCmd->pObj,TABLE,TYPEO)
	        ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->fillvalue(pCmd,&a,VNAME(a.empno),"=",NULL),"");
		free_pArr(pCmd);
//		//setValue2(a,empno,7566);
//		printf("\npppppppppppppppppppppppppppppppppppa.empno=%d\n",a.empno);
//        printf("\npcmd2.str = %s",pCmd->pObj->sqlstr);
		JsonNode* Jnode2 = prepare_executeSQL(conn_prt,pCmd->pObj->sqlstr,pCmd->parModelList,pCmd->retModelList);
//        
//        system("pause");
//		pCmd->createStr(pCmd,pCmd->pObj,GET,NULL)
//		    ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),pCmd->forTable(pCmd,&b,VNAME(b)),"")
//		    ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.empno),VNAME(a.deptno),"")
//		    ->multi_selected(pCmd,&b,pCmd->pObj,TYPEO,VNAME(b.dname),VNAME(b.deptno),"")
//		    ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->compare(pCmd,VNAME(a.deptno),"=",VNAME(b.deptno)),
//			              pCmd->jointWords(pCmd,"AND"),pCmd->fillvalue(pCmd,&b,VNAME(b.loc),"=",NULL),"");
//        printf("\npcmd.str = %s",pCmd->pObj->sqlstr);
        
		
//		SQLObject* obj = createSQLObject(100,COLUMN);
//		pCmd->subQuery(obj,
//		      pCmd->createStr(pCmd,obj,GET)
//		          ->from(pCmd,obj,pCmd->forTable(pCmd,&b,VNAME(b)),"")
//			      ->multi_selected(pCmd,&b,obj,TYPEO,VNAME(b.dname),""));
//		printf("\nsubStr = %s",obj->sqlstr);
//		pCmd->createStr(pCmd,pCmd->pObj,GET)
//            ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
//            ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.empno),VNAME(a.ename),"")
//            ->multi_selected(pCmd,NULL,pCmd->pObj,TYPEN,obj->sqlstr,"");
//        printf("\npcmd.str = %s",pCmd->pObj->sqlstr);

	
//		SQLObject* obj = createSQLObject(100,SUBVIEW);
//	    pCmd->subQuery(obj,
//		      pCmd->createStr(pCmd,obj,GET)
//		          ->from(pCmd,obj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
//			      ->multi_selected(pCmd,&a,obj,TYPEO,VNAME(a.deptno),VNAME(a.ename),""));
//      printf("\nsubStr = %s",obj->sqlstr);
//		pCmd->createStr(pCmd,pCmd->pObj,GET)
//            ->from(pCmd,pCmd->pObj,pCmd->forView(pCmd,obj),pCmd->forTable(pCmd,&b,VNAME(b)),"")
//            ->selectAll(pCmd,NULL,pCmd->pObj,obj,TYPEN)
//			->multi_selected(pCmd,&b,pCmd->pObj,TYPEO,VNAME(b.dname),VNAME(b.deptno),"");   
        printf("\npcmd.str = %s",pCmd->pObj->sqlstr);
        //a.empno = 7369;
        //searchUsers(&a,conn_prt);
//		SQLObject* obj = createSQLObject(100,COLUMN);
//		pCmd->subQuery(obj,
//		      pCmd->createStr(pCmd,obj,GET)
//			      ->from(pCmd,obj,pCmd->forTable(pCmd,&b,VNAME(b)),"")
//			      ->multi_selected(pCmd,&b,obj,TYPEN,VNAME(b.deptno),""));
//		printf("\nsubStr = %s",obj->sqlstr);      
//		pCmd->createStr(pCmd,pCmd->pObj,GET)
//		    ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
//		    ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.empno),VNAME(a.ename),"")
//		    ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->compare(pCmd,VNAME(a.deptno),"in",obj->sqlstr),
//			              pCmd->jointWords(pCmd,"AND"),pCmd->fillvalue(pCmd,&a,VNAME(a.mgr)),"");
//		printf("\npcmd.str = %s",pCmd->pObj->sqlstr);

        
//		pCmd->createStr(pCmd,pCmd->pObj,GET)
//		    ->fromJoin(pCmd,pCmd->pObj,RIGHT_JOIN,pCmd->forTable(pCmd,&a,VNAME(a)),pCmd->forTable(pCmd,&b,VNAME(b)))
//		    ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.empno),VNAME(a.ename),"")
//		    ->multi_selected(pCmd,&b,pCmd->pObj,TYPEO,VNAME(b.dname),VNAME(b.deptno),"")
//		    ->onCondition(pCmd,pCmd->pObj,ON,pCmd->compare(pCmd,VNAME(a.deptno),"=",VNAME(b.deptno)),"");
//      printf("\npcmd.str = %s",pCmd->pObj->sqlstr);

        
//        pCmd->createStr(pCmd,pCmd->pObj,GET)
//            ->from(pCmd,pCmd->pObj,pCmd->forTable(pCmd,&a,VNAME(a)),"")
//            ->multi_selected(pCmd,&a,pCmd->pObj,TYPEO,VNAME(a.sal),VNAME(a.deptno),"")
//            ->onCondition(pCmd,pCmd->pObj,WHERE,pCmd->compare(pCmd,VNAME(a.sal),">","1200"),"")
//            ->group_By(pCmd,pCmd->pObj,VNAME(a.deptno))
//            ->order_By(pCmd,pCmd->pObj,createOrderObj(VNAME(a.deptno),DESC),createOrderObj(VNAME(a.sal),ASC),NULL)
//            ->limit(pCmd,pCmd->pObj,1,10);
//        printf("\npcmd.str = %s",pCmd->pObj->sqlstr);


//	    a.tValue.print(&a);
//	    b.tValue.print(&b);
		//getchar();
//		freeValue(pCmd->parModelList->modelArray,pCmd->parModelList->nLength);
//	    freeValue(pCmd->retModelList->modelArray,pCmd->retModelList->nLength);
	    
		return 0;
}



