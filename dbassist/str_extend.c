/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "str_extend.h"


char *  stristr (
                       const char * str1,
                       const char * str2
                       )
{
    char *cp = (char *) str1;
    char *s1, *s2;
    
    if ( !*str2 )
        return((char *)str1);
    
    while (*cp)
    {
        s1 = cp;
        s2 = (char *) str2;
        
        while ( *s1 && *s2 && toupper(*s1)==toupper(*s2) ){
            s1++, s2++;
        }
        
        if (!*s2)
            return(cp);
        
        cp++;
    }
    
    return(NULL);
    
}


char *  stristr2 (
                       const char * str1,
                       const char * str2
                       )
{
    char *cp = (char *) str1;
    char *s1, *s2, *s3;
    s3 = NULL;
	if ( !*str2 )
        return((char *)str1);
    
    while (*cp)
    {
        s1 = cp;
        s2 = (char *) str2;
        
        while ( *s1 && *s2 && toupper(*s1)==toupper(*s2) ){
            s1++, s2++;
        }
        
        if (!*s2)
            s3 = cp;
        cp++;
    }
    
    return s3;
    
}

int findpos(char* mainstr,char* substr)
{
  char *dest = stristr(mainstr, substr);
  if(dest==NULL)
	  return -1;
  else
      return dest-mainstr;	
}


int findlast(char* mainstr,char* substr)
{
  char *dest = stristr2(mainstr, substr);
  if(dest==NULL)
	  return -1;
  else
      return dest-mainstr;	
}

char* strInsert(char *dst, int index, const char *src){
    int i,l,h,t;
    for(l=0;src[l];l++);
    for(h=0;dst[h];h++);
    if(index>h||index<0)
       return dst;
    for(i=h;i>=index;i--){
       dst[i+l]=dst[i]; 
    }
    for(t=index;src[t-index];t++){
        dst[t]=src[t-index];
    }
    return dst;
}


void getstr(char* src,int start,char exchar,char endchar, char* dest){
  int i = 0;
  char* p = src + start;
  while(*p==exchar)
  {
  	p++;
  }
  while(*p!=endchar)
  {
    dest[i++] = *p;
	p++;	
  }
  dest[i] = '\0';	
}



void substr(const char* src, int start, int end, char* dest) {
  int i;
  int len = end - start + 1;
  for (i = 0; i < len && src[start + i] != '\0'; i++) {
    dest[i] = src[start + i];
  }
  dest[i] = '\0';
}
 
void split(char *src, const char *separator, char **dest, int *num) 
{
    /*
        src 源字符串的首地址(buf的地址) 
        separator 指定的分割字符
        dest 接收子字符串的数组
        num 分割后子字符串的个数
    */
     char *pNext;
     int count = 0;
     
     if (src == NULL || strlen(src) == 0) //如果传入的地址为空或长度为0，直接终止 
        return;
        
     if (separator == NULL || strlen(separator) == 0) //如未指定分割的字符串，直接终止 
        return;
        
     pNext = (char *)strtok(src,separator); //必须使用(char *)进行强制类型转换(虽然不写有的编译器中不会出现指针错误)
     while(pNext != NULL) {
          *dest++ = pNext;
          ++count;
         pNext = (char *)strtok(NULL,separator);  //必须使用(char *)进行强制类型转换
    }  
    *num = count;
}     

void removeCharAt(char* str, int index) {
    int len = strlen(str);
    if (index < 0 || index >= len) {
        printf("Invalid index.\n");
        return;
    }
    int i;
    for (i = index; i < len - 1; i++) {
        str[i] = str[i + 1];
    }
    str[len - 1] = '\0';
}
 
void strTrim(char *pStr){
   char *pTmp = pStr; 
   while(*pStr!='\0')
 {
   if(*pStr!=' ') 
   *pTmp++ = *pStr; 
   ++pStr; 
 }
   *pTmp = '\0'; 
}
 
void strDelChar(char *pStr,char ch){
   char *pTmp = pStr;
   int flag = 0; 
   while(*pStr!='\0')
 {
   if(flag==0)
   {
     if(*pStr=='"')
	 flag = 1;
   }
   else
   {
   	 if(*pStr=='"')
   	 flag = 0;
   }
   if(*pStr!=ch||flag==1) 
   *pTmp++ = *pStr; 
   ++pStr;
 }
   *pTmp = '\0'; 
} 
 
//  a as   d f   fff   ff     ttt   tt e  r    
void strDecBlank(char *pStr,char ch)
{
	char *pTmp = pStr;
	char *qTmp = pTmp;
	int n = 0;
	int flag = 0;
	while(*pStr!='\0')
  {
  	  n = 0;
  	  if(flag==0)
	 {
	    if(*pStr=='"')
		flag = 1;
     }
	  else
	 {
	    if(*pStr=='"')
	   	flag = 0;
	 }
  	  while(*qTmp==ch&&flag==0) //空格 index0 
	 {
	    printf("\nmpn=%d\n",n);
	    n++;
		qTmp=pStr+n;  //a index2
	 }
     if(qTmp-pStr>0)
	 {
	 	printf("\nk=%d\n",qTmp-pStr);
	 	*pTmp++=*pStr;    //' ' 
		printf("\npTmp=%c\n",*pTmp);
		int m = (qTmp-pStr); 
		pStr+=m; //'a'
     }
    	*pTmp++=*pStr;//' 'a
	    pStr++;   
		qTmp=pStr; //' '
  }
  *pTmp = '\0'; 
} 
 
/*去除字符串右边空格*/ 
void strRTrim(char *pStr){
   char *pTmp = pStr+strlen(pStr)-1; 
   while (*pTmp == ' ') {
   *pTmp = '\0'; 
   pTmp--; 
}
}
/*去除字符串左边空格*/ 

void strLTrim(char *pStr){
   char *pTmp = pStr; 
   while(*pTmp==' ') 
   pTmp++; 

   while(*pTmp!='\0')
  {
    *pStr = *pTmp; 
    pStr++; 
    pTmp++; 
  }
  *pStr='\0';  
}

//"INSERT    INTO  t  (a,  b  ,  c  ,  d)      VALUES(   ?,    ?   ,a,  ?  ) "
//"UPDATE t   set  a=  15"
void formattersql(char* mainstr,char** wlibrary,int n)
{
	char *cp = mainstr;
	char keywords[30];
	char *s2,*s1;
	int flag = 0;
	int bflag = 0;
	int i = 0;
	while(*cp)
  {
      s1=cp;
	  flag = 0;
	  for(i=0; i<n; i++)
	  {
		s1 = cp;
		s2 = wlibrary[i];
		while ( *s1 && *s2 && toupper(*s1)==toupper(*s2) ){
            s1++, s2++;
		}
        if (!*s2)
        {
          if(*s1&&*s1!=' ')
		  {
             s1 = cp;
	      }
		  else
		  {
		     if(*s1){
			 if(i<25)
			   flag = 1;
		     else
		       flag = 2;
			 memset(keywords,'\0',30);
		     memcpy(keywords,wlibrary[i],strlen(wlibrary[i]));
			 break;
	        }
	        else
	        {
	          s1 = cp;	
			}
		  }
		}
		else
		{
		  s1 = cp;
		}
        
	  }
      if(flag==1||flag==2)
    {
        while(cp<s1)
      { 
	      if(TinyCall(mainstr,cp,&bflag)<0)
		  return;
	  }
	  if(flag==1)
	  {
	  	  if(TinyCall(mainstr,cp,&bflag)<0)
		  return;
	  }
	}
	    else	
		if(flag==0)
    {
	    if(*s1&&*s1!=' ')
      {
	     memset(keywords,'\0',30);
         s2 = keywords;
         flag = 3;
		 while(*s1&&*s1!=' ')
     	{
		  *s2++=*s1;
		  *mainstr++=*s1;
     	  s1++;
     	  cp = s1;
     	  if(isdigit(*s1)==0&&isalpha(*s1)==0&&*s1!='_')
		  {
		     flag = 4;
		  }
	    }
      }
    }
       
	   if(flag!=1)
    {
       	char words[30];
	  	memset(words,'\0',30);
	  	char* pw = words;
	  	char* chTemp;
	  	int nFlg = 0;
		while(1)
	   {
		    if(*cp==32)
		  {
			char* temp = (cp+1);
			memset(words,'\0',30);
			pw = words;
			nFlg = 0;
			
			while(*temp&&*temp!=32)
	        {
			   *pw++=*temp;
			   if(isalpha(*temp)==0)
			   {
			      nFlg = 1;
			   }
			   chTemp = temp;  
			   temp++;
		    }
			if(nFlg==0)
			{
			   for(i=0; i<n; i++)
	          {
			    if(stricmp(words,wlibrary[i])==0)
			    {
				   nFlg = 2;
				   break;   
			    }
			  }
			  if(nFlg==0)
			  {
			  	nFlg = 1;
			  }
			}
			
			if(nFlg==1)
			{
			    if(*cp==32&&*(cp-1)!=')'&&bflag==0)
		      {
		        cp++;
		      }
				while(cp<chTemp)
              { 
	             if(TinyCall(mainstr,cp,&bflag)<0)
		         return;
	          }
	          if(flag==-1)
	          break;
		    }
		    else
		    if(nFlg==2)
			{
			  if(TinyCall(mainstr,cp,&bflag)<0)
		      return;
			  break;
			}
			
		  }
			if(TinyCall(mainstr,cp,&bflag)<0)
			return;
	   }
	}
     if(flag==0)
	 {
       	if(TinyCall(mainstr,cp,&bflag)<0)
		return;
     }
     
  }
  *mainstr='\0';  
}

void parseSQLStr(char* sqlstr)
{
    strLTrim(sqlstr);
	strRTrim(sqlstr);
	strDecBlank(sqlstr,' ');
	char* keywords[45] = {"INSERT INTO","UPDATE","SET","AND","OR","XOR","DELETE","SELECT","FROM",
    "BETWEEN","CASE","WHEN","ORDER BY","HAVING","GROUP BY","INNER","OUTER","JOIN","ON","LIMIT","DISTINCT","UNION","AS","WHERE",
    "IN","ALL","exists","any","values","value","sum","count","max","min","isnull","DATE_FORMAT","DATE_ADD",
	"DATE_SUB","DATE","concat","REPLACE","SUBSTRING","TRIM","covert","cast"};
    int i,n=0,nIndex=45;
	formattersql(sqlstr,keywords,nIndex); 
    printf("\nsqlstr=%s",sqlstr);
} 


