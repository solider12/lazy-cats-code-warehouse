/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "mysql_extend.h"

enum_field_types convertStringToMySqlType(dataType type)
{
	if(type==C_TIMESTAMP)
	    return MYSQL_TYPE_TIMESTAMP;
	else
	if(type==C_DATETIME)    
	    return MYSQL_TYPE_DATETIME;
	else
	if(type==C_FLOAT)
	    return MYSQL_TYPE_FLOAT; 
	else
	if(type==C_DOUBLE)
	    return MYSQL_TYPE_DOUBLE;
	else
	if(type==C_CHAR) 
	    return MYSQL_TYPE_STRING;
	else
	if(type==C_VARCHAR) 
	    return MYSQL_TYPE_VAR_STRING;
	else
	if(type==C_SHORT)
	    return MYSQL_TYPE_LONG;
	else
	if(type==C_LONG)
	    return MYSQL_TYPE_LONG;    
    else
    if(type==C_INT)
        return MYSQL_TYPE_LONG;
    else    
	return 256;  
}
