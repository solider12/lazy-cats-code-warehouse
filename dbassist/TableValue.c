/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "TableValue.h"

void fillBasic(void* pdata,char* name)
{
	 TableValue* tValue = (TableValue*)pdata;
	 strcpy(tValue->pbValue->tableName,name);
	 strcpy(tValue->pbValue->aliaName,"");
}

void print(void* pdata)
{
	TableValue* tValue = (TableValue*)pdata;
	printf("\nTableName=%s\n",tValue->pbValue->tableName);
	printf("\naliaName=%s\n",tValue->pbValue->aliaName);
	HashMap map = tValue->map;
	HashMapIterator iterator = createHashMapIterator(map);
    while (hasNextHashMapIterator(iterator)) {
        iterator = nextHashMapIterator(iterator);
        model* p = (model*)iterator->entry->value;
        printf("{ key: %s, alianame: %s, fullname: %s, hashcode: %d }\n",
            (char *)iterator->entry->key,p->alianame,p->fullname,iterator->hashCode);
    }
    //map->clear(map);
    freeHashMapIterator(&iterator);
}
