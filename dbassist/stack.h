#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "dataStruct.h"

#define Stack_Size 512  

typedef struct
{
    void** elem;
    int  data_size;
    int  top;
}SeqStack;

void InitStack(SeqStack* S,int data_size);

int Push(SeqStack* S,void* p);

int Pop(SeqStack* S, void* p);

int IsEmpty(SeqStack* S);

int GetPop(SeqStack* S, void* p);

void printStack(SeqStack* S, void (*fptr)(void *));

