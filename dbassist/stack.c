#include "stack.h"


void InitStack(SeqStack* S,int data_size)  //初始化栈
{
    S->top = -1;
    S->data_size = data_size;
    S->elem = (void**)malloc(sizeof(void*)*Stack_Size);
}

int Push(SeqStack* S,void* p)  //入栈
{
    if (S->top == Stack_Size - 1) return False;    //如果栈顶指针等于栈的最大容量减一，说明栈已满，返回False
    S->top++;                                      //否则，将栈顶指针加一
	int i;
    for (i=0; i<S->data_size; i++)
        *(char *)(S->elem+S->top*sizeof(void*)+i) = *(char *)(p + i);
    return True;
}

int Pop(SeqStack* S, void* p)  //出栈
{
    if (S->top == -1) return False;     //如果栈顶指针为-1，说明栈为空，返回False
    else
    {
    	int i;
        for (i=0; i<S->data_size; i++)
      {
		*(char *)(p + i) = *(char *)(S->elem+S->top*sizeof(void*)+i);
      }
		S->top--;                     //将栈顶指针减一
		return True;
    }
}

int IsEmpty(SeqStack* S)   //判断栈是否为空
{
    if (S->top == -1)     //如果栈顶指针为-1，说明栈为空
    {
        return True;    //返回True
    }
    else                 //否则，栈不为空
    {
        return False;   //返回False
    }
}


int GetPop(SeqStack* S, void* p)       //获取栈顶元素
{
    if (S->top == -1) return False;   //如果栈顶指针为-1，说明栈为空，返回False
    else
    {
        p = S->elem[S->top];        //否则，将栈顶元素赋值给x指向的变量
        return (True);               //返回True
    }
}

void printStack(SeqStack* S, void (*fptr)(void *))
{
    if (S->top == -1)                       //如果栈顶指针为-1，说明栈为空
    {
        printf("栈为空\n");          //打印提示信息
        return; //结束函数
    }
 
    printf("栈中的元素: ");                //打印提示信息
    int i;
	for (i = S->top; i >= 0; i--)           //从栈顶到栈底遍历栈中的元素
    {
        (*fptr)(S->elem[i]);       //打印每个元素，用空格隔开
    }
    printf("\n");
}
