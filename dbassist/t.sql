/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.43-log : Database - mydb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mydb`;

/*Table structure for table `dept` */

DROP TABLE IF EXISTS `dept`;

CREATE TABLE `dept` (
  `deptno` int(2) NOT NULL,
  `dname` varchar(14) DEFAULT 'null',
  `loc` varchar(50) DEFAULT 'null',
  PRIMARY KEY (`deptno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `dept` */

insert  into `dept`(`deptno`,`dname`,`loc`) values (10,'Accounting','New York'),(20,'Research','Dallas'),(30,'Sales','Chicago'),(40,'Operations','Boston');

/*Table structure for table `emp` */

DROP TABLE IF EXISTS `emp`;

CREATE TABLE `emp` (
  `empno` int(4) NOT NULL,
  `ename` varchar(10) DEFAULT 'null',
  `job` varchar(10) DEFAULT 'null',
  `mgr` int(4) DEFAULT NULL,
  `hiredate` date DEFAULT NULL,
  `sal` double(7,2) DEFAULT NULL,
  `commm` double(7,2) DEFAULT NULL,
  `deptno` int(2) DEFAULT NULL,
  PRIMARY KEY (`empno`),
  KEY `deptno` (`deptno`),
  CONSTRAINT `emp_ibfk_1` FOREIGN KEY (`deptno`) REFERENCES `dept` (`deptno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emp` */

insert  into `emp`(`empno`,`ename`,`job`,`mgr`,`hiredate`,`sal`,`commm`,`deptno`) values (7369,'Smith','Clerk',7902,'1980-12-17',800.00,NULL,20),(7499,'Allen','Salesman',7968,'1981-02-20',1600.00,300.00,30),(7521,'Ward','Salesman',7968,'1981-02-22',1250.00,500.00,30),(7566,'Jones','Manager',7839,'1981-04-02',2975.00,NULL,20),(7654,'Martin','Salesman',7698,'1981-09-28',1250.00,1400.00,30),(7698,'Blake','Manager',7839,'1981-05-01',2850.00,NULL,30),(7782,'Clark','Manager',7839,'1981-06-09',2450.00,NULL,10),(7788,'Scott','Analyst',7566,'1987-04-19',3000.00,NULL,20),(7839,'King','President',NULL,'1981-11-17',5000.00,NULL,10),(7844,'Turner','Salesman',7698,'1981-09-08',1500.00,0.00,30),(7876,'Adams','Clerk',7788,'1987-05-23',1100.00,NULL,20),(7900,'James','Clerk',7698,'1981-12-03',950.00,NULL,30),(7902,'Ford','Analyst',7566,'1981-12-03',3000.00,NULL,20),(7934,'Miller','Clerk',7782,'1982-01-23',1300.00,NULL,10);

/*Table structure for table `salgrade` */

DROP TABLE IF EXISTS `salgrade`;

CREATE TABLE `salgrade` (
  `grade` int(11) NOT NULL AUTO_INCREMENT,
  `lowsal` int(11) DEFAULT NULL,
  `highsal` int(11) DEFAULT NULL,
  PRIMARY KEY (`grade`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `salgrade` */

insert  into `salgrade`(`grade`,`lowsal`,`highsal`) values (1,700,1200),(2,1201,1400),(3,1401,2000),(4,2001,3000),(5,3001,9999);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
