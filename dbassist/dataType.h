/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#pragma once

#include "stdio.h"

typedef enum filledTypes
{
   FILLED,NOFILLED
}filledTypes;


typedef enum dataType{
	C_SHORT,
	C_INT,
	C_LONG,
	C_FLOAT,
	C_DOUBLE,
	C_DATETIME,
	C_TIMESTAMP,
	C_VARCHAR,
	C_CHAR
}dataType;
