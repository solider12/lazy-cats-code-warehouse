/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#pragma once

#include <stdlib.h>

#include <stdio.h>

#include <string.h>

#include <stddef.h>

#define STRING_SIZE 50

#define PARVAL_SIZE 50

#define RETVAL_SIZE 50 

#define NAME_SIZE 80

#define VALUE_SIZE 80

#define ALIA_NAME_SIZE 60

#define DATE_VALUE_STRING_SIZE 40 

#define  NEWSIZE(type,size)  (type*)malloc(size*sizeof(type))

#define  TinyCall(mainstr,cp,pflag) ({ \
   *mainstr++=*cp; \
   *pflag=*cp&&*cp=='\''==0?*pflag:*pflag==0?1:0; \
   *cp?(cp+=1,1):(*mainstr='\0',-1); \
})

#define  CopyString(dest,src,size) ({  \
   memset(dest,'\0',size);\
   memcpy(dest,src,strlen(src)); \
})

#define VNAME(value) #value

//#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)  
/**
 * container_of - 通过结构体的一个成员获取容器结构体的指针
 * @ptr: 指向成员的指针。
 * @type: 成员所嵌入的容器结构体类型。
 * @member: 结构体中的成员名。
 */
#define container_of(ptr, type, member) ({ \
    const typeof(((type *)0)->member) *__mptr = (ptr); \
    (type *)((char *)__mptr - offsetof(type,member)); \
})  

#define list_entry(ptr, type, member) \
    ((type *)( (char *)(ptr) - (unsigned long)(intptr_t)(&((type*)0)->member)))

static int colN = 97;
static int tableN = 65;
static int viewN = 65;

static int getColN()
{
   return colN++;
}

static void resetColN()
{
   colN = 97;
}

static int getTableN()
{
   return tableN++;
}

static void resetTableN()
{
   tableN = 65;
}

static int getViewN()
{
   return viewN++;
}

static void resetViewN()
{
   viewN = 65;
}


