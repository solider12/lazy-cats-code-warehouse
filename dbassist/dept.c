/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/



#include "dept.h"



static void fillMap(void* pdata)
{
     dept* pvalue = (dept*)pdata;
	 
	 struct_set(pvalue->tValue.map,C_INT,2,pvalue->deptno,&pvalue->deptno,FILLED);
	 struct_set(pvalue->tValue.map,C_CHAR,14,pvalue->dname,pvalue->dname,FILLED);
	 struct_set(pvalue->tValue.map,C_CHAR,50,pvalue->loc,pvalue->loc,FILLED);
	 printf("\nb->loc = %lld",pvalue->loc);
}

static void initMembers(void* pdata)
{
	dept* pvalue = (dept*)pdata;
    pvalue->deptno = 0;
    memset(pvalue->dname,'\0',14);
    memset(pvalue->loc,'\0',50);
}

static void initValue(void* pdata)
{
	dept* pvalue = (dept*)pdata;
	pvalue->tValue.map = createHashMap(NULL, NULL);
	pvalue->tValue.pbValue = NEW(TableBasic);
    pvalue->tValue.fillBasic = fillBasic;
	pvalue->tValue.fillMap = fillMap;
	pvalue->tValue.print = print;
	pvalue->tValue.initMembers = initMembers;
}


dept createDept()
{
	dept p;
	p.tValue.initValue = initValue;
	p.tValue.initValue(&p);
	p.tValue.fillBasic(&p,VNAME(dept));
	p.tValue.initMembers(&p);
	p.tValue.print(&p);
	return p;
}

