#include "time_util.h"

void mysql_time_to_tm(struct tm* t_time,MYSQL_TIME* st)
{
	t_time->tm_year = st->year - 1900;
	t_time->tm_mon = st->month;
	t_time->tm_yday = st->day;
	t_time->tm_hour = st->hour;
	t_time->tm_min = st->minute;
	t_time->tm_sec = st->second;
}

void tm_to_mysql_time(MYSQL_TIME* m_time,struct tm* t_time)
{
	m_time->year = t_time->tm_year + 1900;
	m_time->month = t_time->tm_mon;
	m_time->day = t_time->tm_mday;
	m_time->hour = t_time->tm_hour;
	m_time->minute = t_time->tm_min;
	m_time->second = t_time->tm_sec;
	m_time->second_part = 0;
}

void tm_to_tm(struct tm* d_time,struct tm* s_time)
{
	d_time->tm_year = s_time->tm_year;
	d_time->tm_mon = s_time->tm_mon;
	d_time->tm_mday = s_time->tm_mday;
	d_time->tm_hour = s_time->tm_hour;
	d_time->tm_min = s_time->tm_min;
	d_time->tm_sec = s_time->tm_sec;
}
//获取当前时间戳
long get_current_timestamp(){
    time_t timep;
    time (&timep);
    return timep;
}
//计算两个时间戳的时间差(返回毫秒)
long get_time_difference(long start_time,long end_time){
    return end_time-start_time;
}
//计算两个时间戳的时间差(返回秒)
long get_time_difference_second(long start_time,long end_time){
    return (end_time-start_time)/1000;
}
//计算两个时间戳的时间差(返回分钟)
long get_time_difference_minute(long start_time,long end_time){
    return (end_time-start_time)/60;
}
//计算两个时间戳的时间差(返回小时)
long get_time_difference_hour(long start_time,long end_time){
    return (end_time-start_time)/3600;
}
//计算两个时间戳的时间差(返回天)
long get_time_difference_day(long start_time,long end_time){
    return (end_time-start_time)/86400;
}
//计算两个时间戳的时间差(返回月)
long get_time_difference_month(long start_time,long end_time){
    return (end_time-start_time)/2592000;
}
//计算两个时间戳的时间差(返回年)
long get_time_difference_year(long start_time,long end_time){
    return (end_time-start_time)/31104000;
}

//将时间戳转换为时间
char* get_time_by_timestamp(long timestamp){
    time_t timep = timestamp;
    char* time_str = ctime(&timep);
    return time_str;
}


//时间相加
long add_time(long timestamp,int seconds){
    return timestamp+seconds;
}
//时间相减
long sub_time(long timestamp,int seconds){
    return timestamp-seconds;
}
//时间比较(时间戳) ( timestamp1比timestamp2)(1:大于 0:等于 -1:小于)
int compare_time(long timestamp1,long timestamp2){
    if(timestamp1>timestamp2){
        return 1;
    }else if(timestamp1<timestamp2){
        return -1;
    }else{
        return 0;
    }
}
//格式化时间("%Y-%m-%d %H:%M:%S)
char* format_time(long timestamp,char* format){
    time_t timep = timestamp;
    localtime(&timep);
    char *buffer=(char*)malloc(sizeof(char)*100);
    strftime(buffer,100,format,localtime(&timep));
    return buffer;
}

char* format_tm(struct tm* t_time,char* format)
{
	long long value = mktime(t_time);
	return format_time(value,format);
}

char* format_myTime(MYSQL_TIME* st,char* format)
{
    struct tm t_time;
    t_time.tm_year = st->year - 1900;
	t_time.tm_mon = st->month;
	t_time.tm_yday = st->day;
	t_time.tm_hour = st->hour;
	t_time.tm_min = st->minute;
	t_time.tm_sec = st->second;
	return format_tm(&t_time,format);
}


//获取当前时间的年份
int get_current_year(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_year+1900;
}
//获取当前时间的月份
int get_current_month(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_mon+1;
}
//获取当前时间的日
int get_current_day(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_mday;
}
//获取当前时间的小时
int get_current_hour(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_hour;
}
//获取当前时间的分钟
int get_current_minute(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_min;
}
//获取当前时间的秒
int get_current_second(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_sec;
}
//获取当前时间的星期
int get_current_week(){
    time_t timep;
    time (&timep);
    return localtime(&timep)->tm_wday;
}

//星期转换
char* week_to_str(int week){
    switch (week){
        case 0:
            return "星期日";
        case 1:
            return "星期一";
        case 2:
            return "星期二";
        case 3:
            return "星期三";
        case 4:
            return "星期四";
        case 5:
            return "星期五";
        case 6:
            return "星期六";
        default:
            return "未知";
    }
}

//世界时间转北京时间(需要+8小时)
long utc_to_cst(long timestamp){
    return timestamp+28800;
}

//将标准时间(2022-09-12 13:46:14或者2022/09/12 13:46:14)转换为时间戳
int standard_to_stamp(char *str_time){
    struct tm stm;
    int iY,iM,iD,iH,iMin,iS;
    memset(&stm,0,sizeof(stm));
    iY = atoi(str_time);
    iM = atoi(str_time+5);
    iD = atoi(str_time+8);
    iH = atoi(str_time+11);
    iMin = atoi(str_time+14);
    iS = atoi(str_time+17);
    stm.tm_year=iY-1900;
    stm.tm_mon=iM-1;
    stm.tm_mday=iD;
    stm.tm_hour=iH;
    stm.tm_min=iMin;
    stm.tm_sec=iS;
    return (int)mktime(&stm);
}

//判断平年和闰年(1:平年 0:闰年) 闰年2月29天 平年2月28天    闰年能被4整除但不能被100整除的年份为闰年。能被400整除的为闰年
int is_leap_year(int year){
    if(year%4==0&&year%100!=0||year%400==0){
        return 1;
    }else{
        return 0;
    }
}

//通过时间戳获取季节
char* get_season_by_timestamp(long timestamp){
    int month = get_current_month();
    if(month>=3&&month<=5){
        return "春季";
    }else if(month>=6&&month<=8){
        return "夏季";
    }else if(month>=9&&month<=11){
        return "秋季";
    }else{
        return "冬季";
    }
}

//获取本月第一天的日期时间戳
long get_first_day_of_month(){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    p->tm_mday=1;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}

//获取指定月份的第一天的日期时间戳
long get_first_day_of_month_by_month(int month){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    p->tm_mon=month-1;
    p->tm_mday=1;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}
//获取指定月份最后一天日期时间戳
long get_last_day_of_month_by_month(int month){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    p->tm_mon=month;
    p->tm_mday=0;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}
//获取指定年份的第一天的日期时间戳
long get_first_day_of_month_by_year(int year){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    p->tm_year=year-1900;
    p->tm_mon=0;
    p->tm_mday=1;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}
//获取指定年份最后一天日期时间戳
long get_last_day_of_month_by_year(int year){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    p->tm_year=year-1900;
    p->tm_mon=11;
    p->tm_mday=31;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}

//获取指定日期当周第一天的日期时间戳
long get_first_day_of_week_by_timestamp(long timestamp){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_mday=p->tm_mday-p->tm_wday;
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}
//获取指定日期当周最后一天的日期时间戳
long get_last_day_of_week_by_timestamp(long timestamp){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_mday=p->tm_mday+(6-p->tm_wday);
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}

//获取今天是本月的第几天
int get_day_of_month(){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    return p->tm_mday;
}
//获取今天是本年的第几天
int get_day_of_year(){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    return p->tm_yday;
}
//获取今天是本周的第几天
int get_day_of_week(){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    return p->tm_wday;
}
//获取今天是本季度的第几天
int get_day_of_season(){
    time_t timep;
    time (&timep);
    struct tm *p;
    p=localtime(&timep);
    int month = p->tm_mon;
    int day = p->tm_mday;
    if(month>=0&&month<=2){
        return day;
    }else if(month>=3&&month<=5){
        return day+31;
    }else if(month>=6&&month<=8){
        return day+31+30;
    }else{
        return day+31+30+31;
    }
}
//获取指定时间的开始时间的日期时间戳 (也就是指定时间的00:00:00)
long get_start_time_of_day(long timestamp){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_hour=0;
    p->tm_min=0;
    p->tm_sec=0;
    return mktime(p);
}
//获取指定时间的结束时间的日期时间戳 (也就是指定时间的23:59:59)
long get_end_time_of_day(long timestamp){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_hour=23;
    p->tm_min=59;
    p->tm_sec=59;
    return mktime(p);
}

//添加指定天数
long add_day(long timestamp,int day){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_mday=p->tm_mday+day;
    return mktime(p);
}
//添加指定月数
long add_month(long timestamp,int month){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_mon=p->tm_mon+month;
    return mktime(p);
}
//添加指定年数
long add_year(long timestamp,int year){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_year=p->tm_year+year;
    return mktime(p);
}
//添加指定小时数
long add_hour(long timestamp,int hour){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_hour=p->tm_hour+hour;
    return mktime(p);
}
//添加指定分钟数
long add_minute(long timestamp,int minute){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_min=p->tm_min+minute;
    return mktime(p);
}
//添加指定秒数
long add_second(long timestamp,int second){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_sec=p->tm_sec+second;
    return mktime(p);
}
//添加指定周数
long add_week(long timestamp,int week){
    time_t timep;
    timep = timestamp;
    struct tm *p;
    p=localtime(&timep);
    p->tm_mday=p->tm_mday+week*7;
    return mktime(p);
}
