#pragma once

#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include<stdarg.h>
#include <time.h>
#include <windows.h>
#include <assert.h>
#include <ctype.h> 


#define NEW(type) (type *)malloc(sizeof(type))
#define APPLY(task, ...) task(__VA_ARGS__)
#define ABS(n) n > 0 ? n : -n
#define RANGE(min, max, number) number < min ? min : number > max ? max : number
#define EQUAL(a, b) a == b ? 0 : a < b ? -1 : 1
#define log printf

int isNumber(char* str);

int isNumeric3(char* str);
// 生成uuid
char * createUUID();

void copyTmValue(struct tm* tm1,struct tm* tm2);
// 获取正整数的长度
int getNumberLength(int number);

int gettimeofday(struct timeval *tp, void *tzp);
