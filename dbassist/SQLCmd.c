/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "SQLCmd.h"

struct SQLCmd* multi_selected(struct SQLCmd* pcmd,void* p,SQLObject* pobj,IOTypes type,char* name,...) //有聯係 
{
	char* temp;
	char* val;
	if(p!=NULL)
	  val = selectColumn(pcmd,p,name,type,NULL);
	else
	  val = name;
	addcolumn(pobj,val);
    if(pobj->crudtype==ADD)
    {
      strInsert(val,0,"(");	
	}
	else
	if(pobj->crudtype==PUT)
	{
	  strInsert(val,0,"set ");
	  strcat(val,"=?");	
	}
	strcatValue(pobj,val);
	printf("\nbb1pcmd->sqlStr = %s",pobj->sqlstr);
	va_list args;
	va_start(args,name);
	int i = 1;
	do
	{
	    temp = va_arg(args,char*);
		if(strlen(temp)==0)
		{
		   if(pobj->crudtype==ADD)
		   {
		      strcat(pobj->sqlstr,")");
		      pobj->selectednum+= i; 
	       }
		   break;
	    }
	    char str3[80];
	    memset(str3,'\0',80);
		memcpy(str3,temp,strlen(temp));
		if(p!=NULL)
	      val = selectColumn(pcmd,p,temp,type,NULL);
	    else
	      val = temp;
		addcolumn(pobj,val);
		int n = pobj->fIndex;
		char str[strlen(val)+2];
		memset(str,'\0',strlen(val)+5);
		strcat(str,",");
		if(pobj->crudtype==PUT)
	    {
	      strcat(val,"=?");	
	    }
		strcat(str,val);
		if(n>=0)
		{
		   strInsert(pobj->sqlstr,n-2,str);
		   pobj->fIndex+=strlen(str);
		}
		else
		   strcat(pobj->sqlstr,str);
		i++;   
		//printf("\neeeestrpcmd->sqlStr = %s",str);      
	}while(1);
	va_end(args);
	printf("\n0001pcmd->sqlStr = %s",pobj->sqlstr);
	return pcmd;
}

struct SQLCmd* selectFull(struct SQLCmd* pcmd,void* p,SQLObject* pobj,fromTypes ftype,IOTypes type)
{
	char str3[100];
	memset(str3,'\0',100);
	if(ftype==TABLE)
	{
		TableValue* pVal = (TableValue*)p;
		//printf("asdsadasd22223333");
	    int i = 0;
	   	HashMap map = pVal->map;
	    HashMapIterator iterator = createHashMapIterator(map);
        while (hasNextHashMapIterator(iterator)) 
	  {
        iterator = nextHashMapIterator(iterator);
        model* m = (model*)iterator->entry->value;
		if(m->orgfield==1){
		if(pobj->crudtype!=ADD||(pobj->crudtype==ADD&&m->filltype==FILLED)){
		char* key = (char *)iterator->entry->key;
		char* val = NULL;
        if(p!=NULL)
	      val = selectColumn(pcmd,p,key,type,NULL);
	    else
	      val = key;
        addcolumn(pobj,val);
        if(i>0)
        {
          strcat(str3,",");	
		}
		strcat(str3,val);
		i++;
	   }
       }
	  }
	    freeHashMapIterator(&iterator);
		if(pobj->crudtype==ADD)
		{
		   	strInsert(str3,0,"(");\
		   	strcat(str3,")");
		   	pobj->selectednum+= i;
		}
   	}
	else
	{
	    //printf("asdsadasd1112");
		SQLObject* pobj2 = (SQLObject*)p;
		int j = 0;
	    for(j=0; j<pobj2->colnums; j++)
	  {
	    if(j>0)
		strcat(str3,",");
	    strcat(str3,pobj2->objName);
	    strcat(str3,".");
	    int mk = 0;
		char** pdest = getStrArr(*(pobj2->columns+j),".",mk);
	    strcat(str3,pdest[mk-1]);
	  }
	}
	strcatValue(pobj,str3);
	printf("\n0009999pcmd->sqlStr = %s",pobj->sqlstr);
	return pcmd;
}

struct SQLCmd* values(struct SQLCmd* pcmd,SQLObject* pobj)
{
    char str3[80];
	memset(str3,'\0',80);
	int i = 0;
	for(i=0; i<pobj->selectednum; i++)
    {
      if(i==0)
	  strcat(str3," VALUES(");
	  if(i>0)
	  strcat(str3,",");
	  strcat(str3,"?");
	  if(i==pobj->selectednum-1)
	  strcat(str3,") ");	
	}
	strcat(pobj->sqlstr,str3);
	return pcmd;
}



struct SQLCmd* selectAll(struct SQLCmd* pcmd,void* p,SQLObject* pobj,fromTypes ftype,IOTypes type)
{
	char str3[80];
	memset(str3,'\0',80);
	if(ftype==TABLE)
	{
		TableValue* pVal = (TableValue*)p;
		//printf("asdsadasd23322233");
		HashMap map = pVal->map;
        HashMapIterator iterator = createHashMapIterator(map);
        while (hasNextHashMapIterator(iterator)) 
	    {
        iterator = nextHashMapIterator(iterator);
        model* m = (model*)iterator->entry->value;
        if(m->orgfield==1){
			char* key = (char *)iterator->entry->key;
	        char* val = NULL;
	        if(p!=NULL)
		      val = selectColumn(pcmd,p,key,type,NULL);
		    else
		      val = key;
		    //printf("\nval================================================================ = %s",val);
	        addcolumn(pobj,val);
        }
        }
        freeHashMapIterator(&iterator); 
		strcat(str3,pVal->pbValue->aliaName);
	}
	else
	{
	    SQLObject* apobj = (SQLObject*)p;
		//printf("asdsadasd11129999");
	    strcat(str3,apobj->objName);
	}
	strcat(str3,".*");
	strcatValue(pobj,str3);
  	printf("\npcmd->sqlStr = %s",pobj->sqlstr);
	return pcmd;
}

char* forTable(struct SQLCmd* pcmd,void* p,char* name)
{
	return selectTable(pcmd,p,name);
}

char* forView(struct SQLCmd* pcmd,SQLObject* pobj)   //有聯係 
{
	return selectView(pcmd,pobj);
}

void resetCmd(struct SQLCmd* pcmd)
{
	pcmd->pObj->sIndex = -1;
	pcmd->pObj->fIndex = -1;
	pcmd->pObj->colnums = 0;
	pcmd->pObj->selectednum = 0; 
	memset(pcmd->pObj->sqlstr,'\0',SQL_MAX_SIZE);
	memset(pcmd->pObj->objName,'\0',40); //替代v
	pcmd->tValueMap->removeAll(pcmd->tValueMap);
	freeValue(pcmd->parModelList);
	freeValue(pcmd->retModelList);
}

struct SQLCmd* createStr(struct SQLCmd* pcmd,SQLObject* pobj,crudTypes type,char* format)
{
    switch (type)
	{
	   case ADD:
	   strcat(pobj->sqlstr,"insert into ");
	   break;
	   
	   case DEL:
	   strcat(pobj->sqlstr,"delete ");
	   break;
	   
	   case PUT:
	   strcat(pobj->sqlstr,"update ");
	   break;
	   
	   case GET:
	   {
	     strcat(pobj->sqlstr,"select  ");
	     if(format!=NULL)
		 strcat(pobj->sqlstr,format);
         else
         strcat(pobj->sqlstr,"");
		 pobj->sIndex = findpos(pobj->sqlstr,"select"); 
	   }
	   break;	
	}
	pobj->crudtype = type;
	return pcmd;	
}

struct SQLCmd* onCondition(struct SQLCmd* pcmd,SQLObject* pobj,condTypes type,char* value,...)
{
	char* temp;
	if(type==WHERE)
	   strcat(pobj->sqlstr," where ");
	else if(type==ON)
	   strcat(pobj->sqlstr," on ");
    else if(type==HAVING)
       strcat(pobj->sqlstr," having ");
	strcat(pobj->sqlstr,value);
	va_list args;
	va_start(args,value);
	do
	{
	    temp = va_arg(args,char*);
		if(strlen(temp)==0)
		{
		  //strcat(pobj->sqlstr," ");
	      break;
	    }
	    strcat(pobj->sqlstr," ");
		strcat(pobj->sqlstr,temp);
	}while(1);
	va_end(args);
	return pcmd;
}

struct SQLCmd* group_By(struct SQLCmd* pcmd,SQLObject* pobj,char* value)//group by條件
{
	strcat(pobj->sqlstr," group by ");
	strcat(pobj->sqlstr,value);
	return pcmd;
}

struct SQLCmd* order_By(struct SQLCmd* pcmd,SQLObject* sqlobj,orderObj* pObj,...)//order by條件
{
	orderObj* temp;
	strcat(sqlobj->sqlstr," order by ");
	strcat(sqlobj->sqlstr,pObj->value);
	if(pObj->type==ASC)
	   strcat(sqlobj->sqlstr," ASC");
	else
	   strcat(sqlobj->sqlstr," DESC");
    va_list args;
	va_start(args,pObj);
	do
	{
	    temp = va_arg(args,orderObj*);
		if(temp==NULL)
		{
		  strcat(sqlobj->sqlstr," ");
	      break;
	    }
	    strcat(sqlobj->sqlstr,",");
		strcat(sqlobj->sqlstr,temp->value);
		if(temp->type==ASC)
	        strcat(sqlobj->sqlstr," ASC");
	    else
	        strcat(sqlobj->sqlstr," DESC");
	}while(1);
	va_end(args);
	return pcmd;
}

struct SQLCmd* limit(struct SQLCmd* pcmd,SQLObject* sqlobj,int pageNo,int pageSize)
{
    int from = (pageNo-1)*pageSize;
    char temp[15];
    memset(temp,'\0',15);
	sprintf(temp," limit %d,%d ",from,pageSize);
	strcat(sqlobj->sqlstr,temp);
	return pcmd;
}

char* compare(struct SQLCmd* pcmd,char* val1,char* operation,char* val2)
{
	char* str1 = add_pArr(pcmd,strlen(val1)+strlen(operation)+strlen(val2)+3);
	sprintf(str1,"%s %s %s",val1,operation,val2);
	return str1;
}

char* fillvalue(struct SQLCmd* pcmd,void* p,char* name,char* operation,void* optional)
{
	char* temp = selectColumn(pcmd,p,name,TYPEI,optional);
	char* str2 = add_pArr(pcmd,strlen(temp)+10);
	sprintf(str2,"%s%s?",temp,operation);
	return str2;
}

char* jointWords(struct SQLCmd* pcmd,char* words)
{
	char* str3 = add_pArr(pcmd,strlen(words)+1);
	sprintf(str3,"%s",words);
	return str3;
}

char* forViewPath(struct SQLCmd* pcmd,SQLObject* pobj,char* name) //有聯係 
{
    return forVPath(pcmd,pobj,name);
}

struct SQLCmd* from(struct SQLCmd* pcmd,SQLObject* pobj,char* value1,...)
{
    char* temp;
	if(pobj->fIndex==-1)
	   strcat(pobj->sqlstr," from ");
	else
	   strcat(pobj->sqlstr,",");
	pobj->fIndex = findpos(pobj->sqlstr,"from");
	strcat(pobj->sqlstr,value1);
	printf("\npcmd->sqlStr2 = %s",pobj->sqlstr);
	va_list args;
	va_start(args,value1);
	do
	{
	    temp = va_arg(args,char*);
		if(strlen(temp)==0)
		{
		  break;
	    }
		strcat(pobj->sqlstr,",");
		strcat(pobj->sqlstr,temp);	
	}while(1);
	va_end(args);
	return pcmd;
}

struct SQLCmd* on(struct SQLCmd* pcmd,SQLObject* pobj,char* value1,...)
{
	char* temp;
	strcat(pobj->sqlstr,value1);
	printf("\npcmd->sqlStr2 = %s",pobj->sqlstr);
	va_list args;
	va_start(args,value1);
	do
	{
	    temp = va_arg(args,char*);
		if(strlen(temp)==0)
		{
		  break;
	    }
		strcat(pobj->sqlstr,",");
		strcat(pobj->sqlstr,temp);	
	}while(1);
	va_end(args);
	return pcmd;
}


struct SQLCmd* fromJoin(struct SQLCmd* pcmd,SQLObject* pobj,joinTypes type,char* value1,char* value2)
{
	char* temp;
	strcat(pobj->sqlstr," from ");
	pobj->fIndex = findpos(pobj->sqlstr,"from");
	strcat(pobj->sqlstr,value1);
	switch (type)//INNER_JOIN,LEFT_JOIN,RIGHT_JOIN,CROSS_JOIN
	{
	   case INNER_JOIN:
	   temp = " INNER JOIN ";
	   break;
	   
	   case LEFT_JOIN:
	   temp = " LEFT JOIN ";
	   break;
	   
	   case RIGHT_JOIN:
	   temp = " RIGHT JOIN ";
	   break;
	   
	   case CROSS_JOIN:
	   temp = " CROSS JOIN ";
	   break;	
	}
	strcat(pobj->sqlstr,temp);
	strcat(pobj->sqlstr,value2);
	return pcmd;
}

char* subQuery(SQLObject* pobj)  //
{
    strInsert(pobj->sqlstr,0,"(");
    pobj->sIndex++;
    strcat(pobj->sqlstr,")");
	return pobj->sqlstr;
}

char* selectFormat(selectTypes type)
{
	if(type==DISTINCT)
	   return " DISTINCT";
	else
	   return " ALL";
}


SQLCmd* createSQLCmd()
{
	SQLCmd* cmd = (SQLCmd*)malloc(sizeof(SQLCmd));
	cmd->pObj = createSQLObject(SQL_MAX_SIZE,MAINVIEW);
    cmd->tValueMap = createHashMap(NULL, NULL);
	cmd->parModelList = initModelList();
	cmd->retModelList = initModelList();
	cmd->multi_selected = multi_selected;
	cmd->selectFull = selectFull;
	cmd->selectAll = selectAll;
	cmd->forTable = forTable;
	cmd->forView = forView;
	cmd->from = from;
	cmd->on = on;
	cmd->fromJoin = fromJoin;
	cmd->createStr = createStr;
	cmd->compare = compare;
	cmd->onCondition = onCondition;
	cmd->group_By = group_By;
	cmd->order_By = order_By;
	cmd->limit = limit;
	cmd->values = values;
	cmd->fillvalue = fillvalue;
	cmd->jointWords = jointWords;
	cmd->subQuery = subQuery;
	cmd->selectFormat = selectFormat;
	cmd->forViewPath = forViewPath;
    cmd->nSize = 0;
	return cmd;
}


