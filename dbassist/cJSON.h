/*******************************************************************
    Copyright (c) [2023] [劉國榮]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#pragma once

#include "parseStack.h"
#include "str_extend.h"
#include "dataStruct.h"
#include <assert.h>
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
#define List_Max_Size 1024

#define increase(str,nCurrent) ({\
    while(nCurrent<strlen(str)-1&&(*(str+(nCurrent+1))==','||*(str+(nCurrent+1))==':'||*(str+(nCurrent+1))==']'||*(str+(nCurrent+1))=='}'))\
       ++nCurrent;\
    if(nCurrent<strlen(str)-1)\
	   ++nCurrent;\
})

typedef enum JsonType
{
    Json_Null,
	Json_Object,
	Json_Array,
	Json_Int,
	Json_Double,
	Json_Long,
	Json_String, 	
    Json_Bool 
}JsonType; 


typedef struct JsonNode
{
	char key[20];
	int from;
	int to;
	JsonType type;
	int flag;
	int level;
	int valueint;
	double valuedouble;
	long long valuelong;
	char* valuestring;
	Boolean valuebool;
	struct JsonNode *firstChild;    // 第一个子节点指针
    struct JsonNode *nextSibling;   // 下一个兄弟节点指针
    struct JsonNode *parentNode; 
}JsonNode;

typedef struct JsonNodeIterator
{
    JsonNode* root;
	int nSize;
	struct 	JsonNode* next;
	struct 	JsonNode* current;
}JsonNodeIterator,*JsonIterator;

JsonIterator createJsonIterator(JsonNode* root);

Boolean hasNextIterator(JsonIterator iterator);

void nextIterator(JsonIterator iterator);

void freeJsonIterator(JsonIterator* iterator);

void parseKey(char* mainstr,int from,int to,JsonNode* p);

void parseValue(char* mainstr,int from,int to,JsonNode* p);

void reset(JsonNode *root,int flag);

JsonNode* Find(JsonNode *root,char* key);

void delTree(JsonNode** p);

void convertObjectToString(JsonNode* p,char* strVal);

char* printJson(JsonNode* p);

JsonNode* newJsonItem();

JsonNode* createJsonObject(char* key);

JsonNode* createJsonArray(char* key);

void linkTree(JsonNode* root);

void removeKey(JsonNode* root,char* key);

void addChildJson(JsonNode* object,JsonNode* jsonItem);

void addChildInteger(JsonNode* object,char* key,int value);

void addChildDouble(JsonNode* object,char* key,double value);

void addChildLong(JsonNode* object,char* key,long long value);

void addChildString(JsonNode* object,char* key,char* value);

void LinkNode(JsonNode* node,JsonNode** currNode,char* str);

JsonNode* parseNode(int from,int to,char* str,indexList* L1,indexList* L2,indexList* L3,indexList* L4);

JsonNode* parseTree(char* str);

void DestroyJson(JsonNode* p);


