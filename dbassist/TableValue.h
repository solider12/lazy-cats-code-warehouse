/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/
#pragma once

#include "TableBasic.h"
#include "hashMap.h"
#include "model.h"
#include "str_extend.h"
#include "time_util.h"

#define setMapValue(map,key,value)({\
    model* pobject = Get(map,key);\
    if(pobject->dtype==C_CHAR||pobject->dtype==C_VARCHAR)\
	{\
	   if(value!=NULL)\
	   CopyString((char*)(intptr_t)pobject->addr,(char*)(intptr_t)value,pobject->col_length);\
	}\
    else\
    {\
       if(pobject->dtype==C_DATETIME||pobject->dtype==C_TIMESTAMP)\
      {\
	   if(value!=NULL){\
	   tm_to_tm((struct tm*)(intptr_t)pobject->addr,(struct tm*)(intptr_t)value);}\
      }\
	   else\
	  {\
        if(value!=NULL)\
	   {\
    	   pobject->addr = (long long)value;\
	   }\
	  }\
    }\
    Put(map,pobject->name,pobject);\
})

#define struct_setExtend(map,type,length,fullname,padd,filled)({ \
    model* pobject = Get(map,fullname);\
    if(pobject==NULL){\
	pobject = NEW(model);\
	pobject->parSelected = 0;\
	pobject->retSelected = 0;\
	pobject->dtype = type;\
	pobject->orgfield = 1;\
	pobject->filltype = filled;\
	pobject->sqltype = convertStringToMySqlType(type);\
	pobject->col_length = length;\
	CopyString(pobject->name,fullname,NAME_SIZE);\
	CopyString(pobject->fullname,"",NAME_SIZE);\
	CopyString(pobject->alianame,"",ALIA_NAME_SIZE);\
	if(length>0&&(pobject->dtype==C_CHAR||pobject->dtype==C_VARCHAR)){\
		pobject->data = (char*)malloc(sizeof(char)*pobject->col_length);\
		pobject->col_length++;\
	}\
    pobject->addr = (long long)padd;}\
	Put(map,pobject->name,pobject);\
})


#define struct_set(map,type,length,member,padd,filled)({ \
    int n = 0;\
	char** pdest = getStrArr(VNAME(member),"->",n);\
	char* fullname = pdest[n-1];\
    struct_setExtend(map,type,length,fullname,padd,filled);\
})

typedef struct TableValue{
	TableBasic* pbValue;
	HashMap map;
	void (*initValue)(void* data);
	void (*fillBasic)(void* data,char* name);
	void (*fillMap)(void* data);
	void (*print)(void* data);
	void (*initMembers)(void* data);
}TableValue;

void fillBasic(void* pdata,char* name);

void print(void* pdata);
