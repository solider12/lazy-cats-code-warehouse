C语言mysql预处理助手1.0(mysql prepare dbassist1.0 for c)

介绍
一個C語言封裝mysql預處理的轻量级框架,除了实现对mysql预处理变量绑定代码的封装,还实现了sql语句拼接的封装.整合了mysql預處理單表的crud和多表的聯合查詢以及多表的嵌套查詢。

運行結果

![输入图片说明](../%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-12-04%20171922.png)

預處理的優點
![输入图片说明](../%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202023-12-04%20211253.png)

安装教程
1.安裝mysql環境，參考[<<devc++使用mysql.docx>>](https://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/devc++%E4%BD%BF%E7%94%A8mysql.docx)
2.根據sql表創建結構體，結構體成員變量對應表字段。sql參考[<<t.sql>>](https://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/t.sql)
對應文件參考[emp.c](https://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/emp.c),[dept.c](https://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/dept.c),[salgrade.c](https://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/salgrade.c),
3.將文件打包成.dll或者.a,引入项目,引入方法參考[<<靜態庫打包以及使用教程>>](https://blog.csdn.net/Manofletters/article/details/128695793)

使用说明
參考[<<教程.txt>>](http://gitee.com/solider12/lazy-cats-code-warehouse/blob/master/dbassist/%E6%95%99%E7%A8%8B.txt)

参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/
