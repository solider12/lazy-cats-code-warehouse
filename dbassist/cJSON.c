/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "cJson.h"

JsonIterator createJsonIterator(JsonNode* root)
{
	JsonIterator iterator = NEWSIZE(JsonNodeIterator,1);
	iterator->root = root;
	iterator->current = iterator->root;
	iterator->nSize = 0;
	iterator->next = NULL;
	return iterator;
}

Boolean hasNextIterator(JsonIterator iterator)
{
	  if(iterator->nSize==0)
	  {
	  	 if(iterator->current->firstChild==NULL)
		    return False;
	     else
		 {
		 	iterator->next = iterator->current->firstChild;
		 	return True;
		 }    		   
	  }
	  else
	  {
	  	 if(iterator->current->nextSibling==NULL)
			return False;
		 else
		 {
		 	iterator->next = iterator->current->nextSibling;
		 	return True;
		 }    		   
	  }
}

void nextIterator(JsonIterator iterator)
{
	iterator->current = iterator->next;
	++iterator->nSize;
}

void freeJsonIterator(JsonIterator* iterator)
{
	free(*iterator);
	*iterator = NULL;
}

void parseKey(char* mainstr,int from,int to,JsonNode* p)
{
   if(mainstr[from]=='"'&&mainstr[to]=='"')
   {
       substr(mainstr,from+1,to-1,p->key);
   }	
}

void parseValue(char* mainstr,int from,int to,JsonNode* p)
{
   if(mainstr[from]=='"'&&mainstr[to]=='"')
   {
       p->valuestring = NEWSIZE(char,(to-1)-(from+1)+1);
	   substr(mainstr,from+1,to-1,p->valuestring);
	   p->type = Json_String;
   }
   else
   {
       char nValue[16];
       memset(nValue,'\0',16);
//       printf("\nfrom = %d",from);
//       printf("\nto = %d",to);
       substr(mainstr,from,to,nValue);
 //      printf("\nnValue=%s",nValue);
       double tempd = atof(nValue);
       
       if(tempd - (long long)tempd > 0.00)
       {
       	  p->valuedouble = tempd;
//       	  printf("tempd=%f",p->valuedouble);
       	  p->type = Json_Double;
	   }
	   else
	   {
	   	  if(to-from+1<=10)
	   	  {
			 p->valueint = (int)tempd;
//	   	     printf("tempN=%d",p->valueint);
	         p->type = Json_Int;
		  }
		  else
		  {
		  	 p->valuelong = (long long)tempd;
//	   	     printf("tempL=%lld",p->valuelong);
	         p->type = Json_Long;
		  }
	   }
   }	
}

void reset(JsonNode *root,int flag){
	if(root!=NULL)
	    root->flag = 0;
//		printf("\nroot key==%s",root->key);
//		printf("\nroot level==%d",root->level); 
		if(flag!=0)
		root->level = 0;
		if(root->firstChild!=NULL)
		reset(root->firstChild,flag);
        if(root->nextSibling!=NULL)
		reset(root->nextSibling,flag);
}

JsonNode* Find(JsonNode *root,char* key)
{
	assert(strlen(key)>0);
	if(root==NULL||stricmp(root->key,key)==0)
	{
	  return root;	  
	}
	JsonNode *temp = Find(root->firstChild,key);
	if(temp==NULL)
	temp = Find(root->nextSibling,key);   
	return temp; 
} 

void delTree(JsonNode** p)
{
    if((*p)==NULL)
    return;
    delTree(&(*p)->firstChild);
    delTree(&(*p)->nextSibling);
    if((*p)!=NULL)
    {
      free(*p);
      (*p) = NULL;
	}
}

void convertObjectToString(JsonNode* p,char* strVal)
{
    int flag;
	static int nPos = 0;
	char* str_p = strVal;
//	printf("\nnPos=%d",nPos);
	if(p==NULL)
	return;
	if(nPos==0)
	{
	  if(p!=NULL)
	  reset(p,0);
	}
	while(1)
{
	if(p->type==Json_Object&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+6+1):4;
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\nobject");
	   strcat(temp1,"{}");
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= (strlen(temp1)-1);
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;
	   flag = 0;
	}
	else
	if(p->type==Json_Array&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+6+1):4;
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\narray");
	   strcat(temp1,"[]");
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= (strlen(temp1)-1);
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;
	   flag = 0;
	}
	else
	if(p->type==Json_String&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+3+strlen(p->valuestring)+2+1+1):(strlen(p->valuestring)+2+1+1);
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\nstring"); 
	   int k = strlen(p->valuestring)+3+1;
	   char value[k];
	   memset(value,'\0',k);
	   sprintf(value,"\"%s\"",p->valuestring);
	   strcat(temp1,value);
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= (strlen(temp1)-1);
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;
	   flag = 0;		
	}
	else
	if(p->type==Json_Int&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+3+20+1+1+1):22;
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);	
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\nint");
	   char value[20];
	   memset(value,'\0',20);
	   sprintf(value,"%d",p->valueint);
	   strcat(temp1,value);
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= strlen(temp1)-1;
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;	
	   flag = 0;	
	}
	else
	if(p->type==Json_Long&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+3+20+1+1):22;
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);	
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\nlong");
	   char value[20];
	   memset(value,'\0',20);
	   sprintf(value,"%lld",p->valuelong);
	   strcat(temp1,value);
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= strlen(temp1)-1;
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;	
	   flag = 0;	
	}
	else
	if(p->type==Json_Double&&p->flag==0)
	{
	   int nTemp = strlen(p->key)>0?(strlen(p->key)+3+20+1+1):22;
	   char temp1[nTemp];
	   memset(temp1,'\0',nTemp);	
	   if(strlen(p->key)>0)
	   {
	   	  sprintf(temp1,"\"%s\":",p->key);
	   }
//	   printf("\ndouble");
	   char value[20];
	   memset(value,'\0',20);
	   sprintf(value,"%f",p->valuedouble);
	   strcat(temp1,value);
//	   printf("\nnPos=%d",nPos);
	   strInsert(str_p,nPos,temp1);
	   nPos+= strlen(temp1)-1;
//	   printf("\nnPos=%d",nPos);
//	   printf("\nstrVal=%s",strVal);
	   p->flag = 1;	
	   flag = 0;	
	}
         while(p->firstChild!=NULL&&p->firstChild->flag==0)
	   {
//	   	  printf("\nlchild"); 
	      p = p->firstChild;
	      convertObjectToString(p,strVal);
	     }
	   if(p->nextSibling!=NULL&&p->nextSibling->flag==0)
	   {
//	   	  printf("\nrbrother");
		  p = p->nextSibling;
	   	  strInsert(strVal,nPos+1,",");
	   	  nPos+=2;
	   	  convertObjectToString(p,strVal);
	   }
	   else
	   if(p!=NULL&&p->flag==1)
	   {
	   	     printf("\nanother");
	   	     printf("\np->from=%d",p->from);
             printf("\np->to=%d",p->to);
             flag = 0;
             JsonNode* q = p;
             //printf("\nq level=%d",q->level);
             while(p->parentNode!=NULL)
	    	 {
	    	 	p->flag = 2;
				p = p->parentNode;
				if(p!=NULL&&p->nextSibling!=NULL&&p->nextSibling->flag==0)
	    	 	{
	    	 	   //printf("\nbbbbbbbbbbbbbsssssssssssss");
	    	 	   flag = 1;
	    	 	   //printf("\nrbrother");
	    	 	   //printf("\np level=%d",p->level);
	    	 	   strInsert(strVal,nPos+(p->level-q->level+1),",");
	   	           nPos+=(p->level-q->level+2);
	    	 	   convertObjectToString(p->nextSibling,strVal);
				}
	    	 }
		      
	   }
	      if(flag==0)
       {
	      //printf("\nflag==%d",flag);
	      nPos = 0;
	      return;
       } 
	   
}
}

char* printJson(JsonNode* p)
{
	char str[2048];
    memset(str,'\0',2048);
    convertObjectToString(p,str);
    char* workstr = NEWSIZE(char,strlen(str)+1);
    memset(workstr,'\0',strlen(str)+1);
    memcpy(workstr,str,strlen(str));
    return workstr;
}

JsonNode* newJsonItem()
{
    JsonNode* object = NEWSIZE(JsonNode,1);
    object->firstChild = NULL;
    object->nextSibling = NULL;
    object->parentNode = NULL;
    object->flag = 0;
    object->level = 0;
    memset(object->key,'\0',20);
    return object;
}

JsonNode* createJsonObject(char* key)
{
	JsonNode* pObject = newJsonItem();
	pObject->type = Json_Object;
	memcpy(pObject->key,key,strlen(key));
	return pObject;
}

JsonNode* createJsonArray(char* key)
{
	JsonNode* pArray = newJsonItem();
	pArray->type = Json_Array;
	memcpy(pArray->key,key,strlen(key));
	return pArray;
}

void linkTree(JsonNode* root)
{
	//printf("\nmmmmmmmmp3");
    //printf("\nroot level=%d",root->level);
  	JsonNode* temp = root;
  	if(temp->parentNode==NULL)
	  temp->level = 0;
	else
	{
	  if(temp==temp->parentNode->firstChild)
	 {
      temp->level = temp->parentNode->level - 1;
     }
	  else
	 {
	  temp->level = temp->parentNode->level; 	
	 }    
    }
  	temp->flag = 1;
	while(temp->firstChild!=NULL&&temp->firstChild->flag==0)
	{
		temp->firstChild->level = temp->level-1;
		temp->firstChild->flag = 1;
		temp = temp->firstChild;
	}
	
	if(temp->nextSibling!=NULL&&temp->nextSibling->flag==0)
	{
		temp->nextSibling->level = temp->level;
		temp->nextSibling->flag = 1;
		temp = temp->nextSibling;
		linkTree(temp);
	}
	
	while(temp->parentNode!=NULL)
	{
	    temp = temp->parentNode;
	    if(temp->nextSibling!=NULL&&temp->nextSibling->flag==0)
	    {
	    	linkTree(temp->nextSibling); 
		}
	}
	//printf("\n90001!!");	
}


void removeKey(JsonNode* root,char* key)
{
    JsonNode* p = Find(root,key);
	if(p!=NULL)
	{
	   //printf("\n�ҵ���!!");
	   if(p->firstChild!=NULL)
	      delTree(&p->firstChild);
	   if(p->nextSibling!=NULL)
	   {
	       if(p==p->parentNode->firstChild)
	       {
	       	  p->parentNode->firstChild = p->nextSibling; 
		   }
		   else
		   {
		   	  p->parentNode->nextSibling = p->nextSibling; 
		   }
		   p->nextSibling->parentNode = p->parentNode;
		   if(p!=NULL)
	       free(p);
	   }
	   else
	   {
	       p->parentNode->firstChild = NULL;
	       p->parentNode->nextSibling = NULL;
		   delTree(&p);
	   }   
	}
	if(root!=NULL)
	{
	  //printf("\nreset start!!");
	  reset(root,1);
	  //printf("\nreset end!!");
      linkTree(root);
    }
}


void addChildJson(JsonNode* object,JsonNode* jsonItem)
{
	if(strlen(jsonItem->key)>0&&Find(object,jsonItem->key)!=NULL)
	{
		//printf("\nkey has already exists!");
		return;
	}
	if(object->firstChild==NULL)
	{
	  object->firstChild = jsonItem;
	  jsonItem->parentNode = object;
	}
	else
	{
		JsonNode *child = object->firstChild;
		while(child->nextSibling!=NULL)
		{
			child = child->nextSibling;
		}
		child->nextSibling = jsonItem;
		jsonItem->parentNode = child;
	}
	//printf("\nreset start!!");
	reset(object,1);
	//printf("\nreset end!!");
    linkTree(object);
}


void addChildInteger(JsonNode* object,char* key,int value)
{
	JsonNode* child = newJsonItem();
	memcpy(child->key,key,strlen(key));
	child->valueint = value;
	child->type = Json_Int;
	addChildJson(object,child);
}

void addChildDouble(JsonNode* object,char* key,double value)
{
	JsonNode* child = newJsonItem();
    memcpy(child->key,key,strlen(key));
	child->valuedouble = value;
	child->type = Json_Double;
	addChildJson(object,child);
}

void addChildLong(JsonNode* object,char* key,long long value)
{
	JsonNode* child = newJsonItem();
	memcpy(child->key,key,strlen(key));
	child->valuelong = value;
	child->type = Json_Long;
	addChildJson(object,child);
}

void addChildString(JsonNode* object,char* key,char* value)
{
	JsonNode* child = newJsonItem();
	memcpy(child->key,key,strlen(key));
	child->valuestring = NEWSIZE(char,strlen(value)+1);
	memset(child->valuestring,'\0',strlen(value)+1);
	memcpy(child->valuestring,value,strlen(value));
	child->type = Json_String;
	addChildJson(object,child);
}

void LinkNode(JsonNode* node,JsonNode** currNode,char* str)
{
    node->firstChild = NULL;
	node->nextSibling = NULL;
	node->flag = 0;
	node->level = 0;	
	if(node->from==0&&node->to==strlen(str)-1)
    {
       node->parentNode = NULL;
       node->level = 0;
       *currNode = node;
       //printf("\nobject full!!");
	}
	else
	{
	   //printf("\nobject local!!"); 
	   if((*currNode)->from<node->from&&(*currNode)->to>node->to)
	   {
	   	  //printf("\nchild!!"); 
		  node->parentNode = (*currNode);
	   	  node->level = (*currNode)->level-1;
		  (*currNode)->firstChild = node;
	   	  (*currNode) = (*currNode)->firstChild; 
	   }
	   else
	   {
	   	  //printf("\nbrother!!"); 
		  node->parentNode = (*currNode);
		  node->level = (*currNode)->level;
	   	  (*currNode)->nextSibling = node;
	   	  (*currNode) = (*currNode)->nextSibling;
	   }
	   *currNode = node;
	   JsonNode* temp = node;
	   if(node->type!=Json_Object&&node->type!=Json_Array)
	 {
	   while(temp->parentNode!=NULL)
	   {
	   	  temp = temp->parentNode;
	   	  if(node->to+1==temp->to)
	   	  {
		     *currNode = node;
		     //printf("\nvalue return!!");
		     break;
		  }
	   }
	   JsonNode* temp = node;
	   while(temp->parentNode!=NULL)
	   {
	   	  temp = temp->parentNode;
		  if((*currNode)->to+1==temp->to)
		  {
		  	 *currNode = temp;
		     //printf("\nobject or array return!!");
		  }
	   }
	 }
	}
}

JsonNode* parseNode(int from,int to,char* str,indexList* L1,indexList* L2,indexList* L3,indexList* L4)
{
		int nCurrent = from;
		int nTemp;
		JsonNode* root = NULL;
		JsonNode* currNode = NULL;
		while(nCurrent<to)
     { 	
		nTemp=getnRIndex(nCurrent,L1);
		if(nTemp!=-1)
	    {
	    	//printf("\njson object!!");
	    	if(nTemp<=to)
	    	{
	    	   JsonNode* node = NEWSIZE(JsonNode,1);
			   memset(node->key,'\0',20); 
			   if(nCurrent-1>0&&str[nCurrent-1]==':')
	    	  {
	    	    int to = nCurrent - 2;
				int from = getnLIndex(to,L3);
				parseKey(str,from,to,node);
				//printf("\njson object key=%s",node->key);
			  }
			    node->type = Json_Object;
			    node->from = nCurrent;
			    node->to = nTemp;
			    //printf("\njson object from = %d",node->from);
				//printf("\njson object to = %d",node->to);
				LinkNode(node,&currNode,str);
				if(currNode->parentNode==NULL)
				{
			       //printf("\nroot");
				   root = currNode;	
				}    
			}
	    	++nCurrent;
	        continue; 
		}
		nTemp=getnRIndex(nCurrent,L2);
		if(nTemp!=-1)
	    {
	        //printf("\njson array!!");
	        //printf("str1=%c",str[nTemp+1]);
	        //printf("str2=%c",str[nCurrent-1]);
	        if(nTemp<=to)
	    	{
	    	    JsonNode* node = NEWSIZE(JsonNode,1);
			    memset(node->key,'\0',20); 
			   	if(nCurrent-1>0&&str[nCurrent-1]==':')
	    	  {
	    	    int to = nCurrent - 2;
				int from = getnLIndex(to,L3);
				parseKey(str,from,to,node);
				//printf("\njson array key=%s",node->key);
			  }
			    node->type = Json_Array;
			    node->from = nCurrent;
			    node->to = nTemp;
			    //printf("\njson array from = %d",node->from);
				//printf("\njson array to = %d",node->to);
				LinkNode(node,&currNode,str);    
			}
	        ++nCurrent;
		    continue; 
		}
		nTemp=getnRIndex(nCurrent,L3);
	    if(nTemp!=-1)
	    {
	    	//printf("\njson string!!");
	        JsonNode* node = NEWSIZE(JsonNode,1);
			memset(node->key,'\0',20); 
			if(nTemp+1<=to&&str[nTemp+1]==':')
		   {
		       nCurrent = nTemp;
		       //printf("\njson string key jump;");
		   }
		    else
		   {
		   	 if(nCurrent-1>0&&str[nCurrent-1]==':')
	    	  {
	    	    int to = nCurrent - 2;
				int from = getnLIndex(to,L3);
				parseKey(str,from,to,node);
				//printf("\njson string key=%s",node->key);
			  }
		       node->from = nCurrent;
			   node->to = nTemp;
			   nCurrent = nTemp;
			   //printf("\njson string from = %d",node->from);
			   //printf("\njson string to = %d",node->to);
			   parseValue(str,node->from,node->to,node);
			   //printf("\njson string value = %s",node->valuestring);
			   LinkNode(node,&currNode,str);
		   }   
			   increase(str,nCurrent);
			   continue;
		}
		nTemp=getnRIndex(nCurrent,L4);
		if(nTemp!=-1)
		{
            //printf("\njson numeric!!");
            //printf("\nnsssssCurrent2=%d",nCurrent);
	        JsonNode* node = NEWSIZE(JsonNode,1);
			memset(node->key,'\0',20); 
			if(nTemp+1<=to&&str[nTemp+1]==':')
		   {
		       nCurrent = nTemp;
			   //printf("\njson numeric key jump");
		   }
		   else
		   {
		   	 if(nCurrent-1>0&&str[nCurrent-1]==':')
	    	  {
	    	    int to = nCurrent - 2;
				int from = getnLIndex(to,L3);
				parseKey(str,from,to,node);
				//printf("\njson numeric key=%s",node->key);
			  }
		       node->from = nCurrent;
			   node->to = nTemp;
			   nCurrent = nTemp;
			   //printf("\njson numeric from = %d",node->from);
			   //printf("\njson numeric to = %d",node->to);
			   parseValue(str,node->from,node->to,node);
			   //if(node->type==Json_Long)
			       //printf("\njson numeric value = %lld",node->valuelong);
			   //else
			   //if(node->type==Json_Int)
			       //printf("\njson numeric value = %d",node->valueint);
			   //else
			       //printf("\njson numeric value = %d",node->valuedouble);
			   LinkNode(node,&currNode,str);    
			}   
			   increase(str,nCurrent);
			   continue;
		}
     }
//     //printf("\ncurrNode->from=%d",currNode->from);
//     //printf("\ncurrNode->to=%d",currNode->to);
	 return root;
}

JsonNode* parseTree(char* str)
{
	char str2[2048];
    memset(str2,'\0',2048);
    memcpy(str2,str,strlen(str));
    strDelChar(str2,'\t');
    strDelChar(str2,' ');
    strDelChar(str2,'\n');
    strDelChar(str2,'\r');
    indexList* L1 = createIndexList(Braceses);
    if(BracketMatch(str2,L1))
    {
        if(L1->length>0)
		   sortList(L1);
		else
		   return NULL;
    }
    indexList* L2 = createIndexList(Brackets);
    if(BracketMatch(str2,L2))
    {
        sortList(L2);
    }
    indexList* L3 = createIndexList(DouQuots);
    if(BracketMatch(str2,L3))
    {
        sortList(L3);
    }
    indexList* L4 = createIndexList(Numerics);
    NumberMatch(str2,L4);

    JsonNode* temp  = parseNode(0,strlen(str2)-1,str2,L1,L2,L3,L4);
    printf("\nstr2=%s",str2);
    return temp;
}

void DestroyJson(JsonNode* p)
{
	delTree(&p);
} 

