/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#pragma once

#include "common.h"

#define getStrArr(str,flag,n)({\
       char* pdest[2];\
       char str2[20];\
	   CopyString(str2,str,20);\
	   split(str2,flag,pdest,&n);\
	   pdest;\
})


char* stristr(const char * str1,const char * str2);

int findpos(char* mainstr,char* substr);

int findlast(char* mainstr,char* substr);

char* strInsert(char *dst, int index, const char *src);

void getstr(char* src,int start,char exchar,char endchar, char* dest);

void substr(const char* src, int start, int end, char* dest);
 
void split(char *src, const char *separator, char **dest, int *num); 

void strTrim(char *pStr);

void removeCharAt(char* str, int index);

void strDelChar(char *pStr,char ch);

void strDecBlank(char *pStr,char ch);
/*ȥ���ַ����ұ߿ո�*/ 
void strRTrim(char *pStr);
/*ȥ���ַ�����߿ո�*/ 
void strLTrim(char *pStr);

void formattersql(char* mainstr,char** wlibrary,int n);

void parseSQLStr(char* sqlstr);
