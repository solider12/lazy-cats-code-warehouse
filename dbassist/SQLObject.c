/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/

#include "SQLObject.h"

SQLObject* createSQLObject(int size,sqlTypes type)
{
	SQLObject* pObj = NEWSIZE(SQLObject,1); 
	pObj->type = type;
	pObj->sIndex = -1;
	pObj->fIndex = -1;
	pObj->colnums = 0;
	pObj->selectednum = 0; 
	pObj->sqlstr = NEWSIZE(char,size);
	memset(pObj->sqlstr,'\0',size);
	memset(pObj->objName,'\0',40); //���viewName
	return pObj;
}


void addcolumn(SQLObject* pObj,char* p)
{
    if(pObj->type==SUBVIEW)
  {
	 *(pObj->columns + pObj->colnums) = p;
	 pObj->colnums++;
  }
}
	
