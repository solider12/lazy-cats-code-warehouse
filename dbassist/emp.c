/*******************************************************************
    Copyright (c) [2023] [�����s]
   [dbassist] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2. 
   You may obtain a copy of Mulan PSL v2 at:
               http://license.coscl.org.cn/MulanPSL2 
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
   See the Mulan PSL v2 for more details.   
 *******************************************************************/


#include "emp.h"

static void fillMap(void* pdata)
{
     emp* pvalue = (emp*)pdata;
	 
	 struct_set(pvalue->tValue.map,C_INT,4,pvalue->empno,&pvalue->empno,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_CHAR,10,pvalue->ename,pvalue->ename,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_CHAR,10,pvalue->job,pvalue->job,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_INT,4,pvalue->mgr,&pvalue->mgr,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_DATETIME,64,pvalue->hiredate,pvalue->hiredate,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_DOUBLE,9,pvalue->sal,&pvalue->sal,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_DOUBLE,9,pvalue->commm,&pvalue->commm,FILLED);
	 
	 struct_set(pvalue->tValue.map,C_INT,2,pvalue->deptno,&pvalue->deptno,FILLED);
}

static void initMembers(void* pdata)
{
	emp* pvalue = (emp*)pdata;
	pvalue->empno = 0;
	pvalue->mgr = 0;
	pvalue->deptno = 0;
    memset(pvalue->job,'\0',10);
    memset(pvalue->ename,'\0',10);
    pvalue->commm = 0.00;
    pvalue->sal = 0.00;
	pvalue->hiredate = (struct tm*)malloc(sizeof(struct tm));
	memset((char*)pvalue->hiredate,0,sizeof(struct tm));
    pvalue->hiredate->tm_year = 1980-1900;
    pvalue->hiredate->tm_mon = 12;
    pvalue->hiredate->tm_mday = 17;
    pvalue->hiredate->tm_hour = 14;
    pvalue->hiredate->tm_min = 0;
    pvalue->hiredate->tm_sec = 1;
    pvalue->hiredate->tm_isdst = 0;
}

static void initValue(void* pdata)
{
	emp* pvalue = (emp*)pdata;
    pvalue->tValue.map = createHashMap(NULL, NULL);
	pvalue->tValue.pbValue = NEW(TableBasic);
	pvalue->tValue.fillBasic = fillBasic;
	pvalue->tValue.fillMap = fillMap;
	pvalue->tValue.print = print;
	pvalue->tValue.initMembers = initMembers;
}

emp createEmp()
{
	emp p;
	p.tValue.initValue = initValue;
	p.tValue.initValue(&p);
	p.tValue.fillBasic(&p,VNAME(emp));
	p.tValue.initMembers(&p);
   	p.tValue.print(&p);
   	return p;
}

